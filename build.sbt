name := "exalted-charm-server Root"

scalaVersion in ThisBuild := "2.11.8"


val protocolurl =  "https://bitbucket.org/nephtys/exalted-charm-protocol.git"
val openidhelperurl = "https://nephtys@bitbucket.org/nephtys/scala-openidconnect-authentication-helpers.git"


lazy val protocolProject = RootProject(uri(protocolurl))
lazy val openidhelperProject = RootProject(uri(openidhelperurl))

val akkaV = "2.4.11"
val scalaTestV = "3.0.0"


lazy val root = project.in(file(".")).
  dependsOn(protocolProject, openidhelperProject).
  aggregate(charmserverJS, charmserverJVM).
  settings(
    publish := {},
    publishLocal := {}
    //,run in Compile <<= (run in Compile in charmserverJVM)
  )

lazy val charmserver = crossProject.in(file(".")).
  settings(
    name := "exalted-charm-server",
    organization   := "nephtys",
    version := "0.1-SNAPSHOT",
    scalaVersion := "2.11.8"
    ,libraryDependencies += "org.bitbucket.nephtys" %%% "exalted-charm-protocol" % "0.1-SNAPSHOT"
  ).
  jvmSettings(
    // Add JVM-specific settings here
    libraryDependencies ++= {
      Seq(
        "com.typesafe.akka" %% "akka-actor" % akkaV,
        "com.typesafe.akka" %% "akka-stream" % akkaV,
        "com.typesafe.akka" %% "akka-http-experimental" % akkaV,
        "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaV,
        "com.typesafe.akka" %% "akka-http-testkit" % akkaV,
        "org.scalatest"     %% "scalatest" % scalaTestV % "test",
        "com.typesafe.akka" %% "akka-cluster-sharding"  % akkaV,
        "com.typesafe.akka" %% "akka-cluster-tools" % akkaV,
        "com.typesafe.akka" %% "akka-persistence-query-experimental" % "2.4.11",
        "com.typesafe.akka" %% "akka-persistence" % "2.4.11",
        "org.iq80.leveldb"            % "leveldb"          % "0.7",
        "org.fusesource.leveldbjni"   % "leveldbjni-all"   % "1.8",
        "com.typesafe" % "config" % "1.3.1",
        "com.github.cb372" %% "scalacache-caffeine" % "0.9.3",
        "com.lihaoyi" %% "upickle" % "0.4.3",
        "com.google.code.findbugs" % "jsr305" % "3.0.1" % "compile"
      )},
    scalacOptions ++= Seq("-deprecation", "-encoding", "UTF-8", "-feature", "-target:jvm-1.8", "-unchecked",
      "-Ywarn-adapted-args", "-Ywarn-value-discard", "-Xlint"),
    javacOptions ++= Seq("-Xlint:deprecation", "-Xlint:unchecked", "-source", "1.8", "-target", "1.8", "-g:vars")
  ).
  jsSettings(
    scalacOptions ++= Seq("-deprecation", "-encoding", "UTF-8", "-feature", "-target:jvm-1.8", "-unchecked",
      "-Ywarn-adapted-args", "-Ywarn-value-discard", "-Xlint"),
    jsDependencies += "org.webjars.npm" % "rxjs" % "5.0.0-rc.2" / "bundles/Rx.min.js" commonJSName "Rx",
    jsDependencies += "org.webjars" % "jquery" % "2.2.1" / "jquery.js" minified "jquery.min.js",

      // Add JS-specific settings here
    libraryDependencies ++= Seq(
      "io.github.widok" %%% "widok" % "0.2.2",
      //"org.scala-js" %%% "scalajs-dom" % "0.9.1",
      "org.scalatest" %%% "scalatest" % "3.0.0" % "test",
      "com.github.lukajcb" %%% "rxscala-js" % "0.7.0",
      "com.github.marklister" %%% "base64" % "0.2.3",
      "com.lihaoyi" %%% "upickle" % "0.4.3",
      "org.querki" %%% "jquery-facade" % "1.0"
    )
    //,unmanagedSourceDirectories in  <++= unmanagedSourceDirectories in

  )

lazy val charmserverJVM = charmserver.jvm.dependsOn(openidhelperProject)//.dependsOn(protocolProject)
lazy val charmserverJS = charmserver.js//.dependsOn(protocolProject)


//lazy val jsproject = project.dependsOn(charmserverJS, protocolProject).enablePlugins(ScalaJSPlugin)
//lazy val jvmproject = project.dependsOn(charmserverJVM)