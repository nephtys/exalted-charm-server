package loom.charm

import loom.charm.pages.Routes
import org.widok.Buffer
import org.widok.bindings.Bootstrap.Table
import nephtys.loom.charm.protocol.Power
import org.widok._
import org.widok.bindings.HTML
import org.widok.Page
import org.widok.html._
import org.widok.bindings.Bootstrap._
import org.widok.bindings.HTML.Anchor

/**
  * Created by nephtys on 11/16/16.
  */
object TableMapper {

  protected def buildRow(power : Power) /*: Table.Row*/ = {
    Table.Row(td(power.name), td(power.uuid.id.toString), td(power.readableType)).style(Style.Info).onClick(f => Routes.details("param", power.uuid.id).go())
  }

  def buildTable(buf : DeltaBuffer[Power]
                // , clickHandler : Power => Unit
                ) : Table = {
    Table(
      thead(
        tr(
          th("name"),
          th("uuid"),
          th("powertype")
        )
      ),
      tbody(
        buf.map(power => buildRow(power))
      )
    )
  }
}
