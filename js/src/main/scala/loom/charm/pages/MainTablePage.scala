package loom.charm.pages


import java.util.Date

import loom.charm.{DataLayer, TableMapper}
import nephtys.loom.charm.protocol.{Charm, Evocation, Power, Repurchase}
import org.widok.{Page, _}
import org.widok.bindings.Bootstrap.Input.Text
import org.widok.bindings.Bootstrap._
import org.widok.bindings.HTML
import org.widok.bindings.HTML.Anchor
import org.widok.html._

/**
  * Created by nephtys on 11/16/16.
  */
case class MainTablePage(dataLayer: DataLayer)()   extends Page {

  private val searchfield = Var[String]("")

  private val searchfieldInBuffer = Buffer[String]("")
  dataLayer.powers.changes.attach(d => {
    if(!searchfieldInBuffer.get.head.equals(searchfield.get)) {
    println("Data source changed...")
    searchfieldInBuffer.clear()
    searchfieldInBuffer.+=(searchfield.get)
    }
  })


  private def searchFieldChanged() : Unit = {
    if(!searchfieldInBuffer.get.head.equals(searchfield.get)) {
    println("Searchfield changed...")
    searchfieldInBuffer.clear()
    searchfieldInBuffer.+=(searchfield.get)
  }
  }


  private val filteredbuffer = searchfieldInBuffer.flatMap(s => dataLayer.powers).filter(p => {
    val s = searchfield.get
    s.isEmpty || (p.description.description.contains(s)
      || p.name.contains(s)
      || p.keywords.exists(_.title.contains(s)) || p.uuid.id.contains(s)
      || categoryContains(p, s))
  })


  private def categoryContains(p : Power, s : String) : Boolean  = {
    p match {
      case charm: Charm => charm.category.str.contains(s)
      case evo : Evocation => evo.category.str.contains(s)
      case _ => false
    }
  }




  def view() = HTML.Section(
    h1("Charm Main Page"),
    Anchor(Button("Login").style(Style.Primary).show(dataLayer.tokenservice.inMemoryToken.is(None))).url(dataLayer.httpservice.loginURL),
    Button(dataLayer.tokenservice.inMemoryEmail.map(o => s"Authenticated as ${o.map(_.email).getOrElse("N/A")}, tap to logout"))
      .style(Style.Info).show(dataLayer.tokenservice.inMemoryToken.isNot(None))
      .onClick(f => dataLayer.logoutButtonPressed()),
    div(label("current authentication header value: "), Text().subscribe(dataLayer.tokenservice.inMemoryTokenWithBearer).enabled(false)),
    div(label(dataLayer.tokenservice.inMemoryTokenExpiring.map(l => s"Expires at ${new Date(l)}"))).show(dataLayer.tokenservice.inMemoryTokenExpiring.isNot(0)),
    div(hr(),label("You seem to be offline currently! No connection to server. Will try synchronizing local changes once connection is reestablishment. You can still edit local data. Remote changes in the meantime will be later merged before any local changes you make."), hr()).show(dataLayer.stateManager.communicationManager.online.isNot(true)),
    hr(),
    Anchor(Button("New Charm or Spell").style(Style.Success)).url(Routes.create()).enabled(dataLayer.tokenservice.inMemoryEmail.isNot(None)),
    hr(),
    div(
    div(
      div(
        Input.Text().attribute("class", "form-control").placeholder("Search for...").bind(searchfield).onKeyPress(e => {
          if(e.key == "Enter") searchFieldChanged()
        }) ,
        span(
          Button("Go!").style(Style.Default).onClick(f => searchFieldChanged())
        ).attribute("class", "input-group-btn")
      ).attribute("class", "input-group")
    ).attribute("class", "col-lg-6")
    ).attribute("class", "row"),
    hr(),
    TableMapper.buildTable(filteredbuffer)
  )

  def ready(route: InstantiatedRoute) {
    log(s"Page 'main' loaded with route '$route'")
  }

  override def destroy() {
    log("Page 'main' left")
  }
}
