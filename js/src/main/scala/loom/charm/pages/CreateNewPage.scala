package loom.charm.pages

import loom.charm.DataLayer
import nephtys.loom.charm.protocol.CharmProtocol.Create
import nephtys.loom.charm.protocol.{Email, PowerType, UniquePowerIdentifier}
import nephtys.loom.charm.protocol.PowerType.PowerType
import org.scalajs.dom
import org.widok.Buffer.Delta
import org.widok.Page
import org.widok._
import org.widok.bindings.{Bootstrap, HTML}
import org.widok._
import org.widok.bindings.HTML
import org.widok.Page
import org.widok.html._
import org.widok.bindings.Bootstrap._
import org.widok.bindings.HTML.Anchor

/**
  * Created by nephtys on 11/16/16.
  */
case class CreateNewPage(dataLayer: DataLayer)() extends Page {
  private val nameVar : Var[String]         = Var("")
  private val precVar : Var[String]         = Var("")
  private val powertypes : ReadBuffer[String]  = Buffer(PowerType.Charm.toString, PowerType.Evocation.toString, PowerType.Spell.toString, PowerType.CharmRepurchase.toString)
  private val powermap : Map[String, PowerType] = Seq(PowerType.Charm, PowerType.Evocation, PowerType.Spell, PowerType.CharmRepurchase).map(a => (a.toString, a)).toMap
  private val typeVar : Var[Option[String]] = Var(Some(powertypes(0)))

  def view() = div(
    div(
      h1("Create a new Power (including Charms and Spells)"),
      div(
        label("Name:").attribute("for", "newnameid"),
        text().placeholder("Enter new Power name").attribute("class", "form-control").attribute("id", "newnameid").bind(nameVar)
      ).attribute("class", "form-group"),
      div(
        label("Select type of Power:").attribute("for", "newtypeid"),
        select().bind(powertypes, option, typeVar).attribute("class", "form-control").attribute("id", "newtypeid")
      ).attribute("class", "form-group"),
      div(
        label("For Repurchases, write in ID for precursor:").attribute("for", "newprecid"),
        text().placeholder("Enter precursor id").attribute("class", "form-control").attribute("id", "newprecid").bind(precVar)
      ).attribute("class", "form-group").visible(typeVar.is(Some(PowerType.CharmRepurchase.toString)))
    ),
    a(Button("Cancel and back to Main").style(Bootstrap.Style.Danger)).url(Routes.main()),
    Button("Save new Power").style(Bootstrap.Style.Success).visible(nameVar.isNot(""))
        //.enabled(dataLayer.tokenservice.inMemoryEmail.isNot(None))
      .onClick(_ => savePressed())
  )

  def savePressed() : Unit = {
    println("save pressed")
    var goMain = true
    typeVar.get.flatMap(s => powermap.get(s)).foreach(power => {
      val precCharm : Option[UniquePowerIdentifier] = if (typeVar.get.exists(_.equals(PowerType.CharmRepurchase.toString))) {None} else {
        Some(UniquePowerIdentifier(precVar.get))
      }
      goMain = false
      val uuid = UniquePowerIdentifier( dataLayer.randomID())

      val email = dataLayer.tokenservice.myEmail.getOrElse({
        Email(dom.window.prompt("You are currently offline and not logged in. If you want to create a new " +
          "power we need your googlemail address to mark you as the author. As long as you do not explicitly " +
          "define the power as public later on nobody else can see your address. If you enter someone else's address " +
          "your newly created power will be thrown away once you reestablish connections","myemail@gmail.com"))
      })
      dataLayer.tokenservice.myEmail.foreach(email => dataLayer.enqeueCommand(Create(email, nameVar.get, power, uuid, precCharm)))
      Routes.details("param", uuid.id).go()
    })
    if(goMain) {
      Routes.main().go()
    }
  }

  def ready(route: InstantiatedRoute) {
    log(s"Page 'new' loaded with route '$route'")
    nameVar.:=("")
    precVar.:=("")
  }

  override def destroy() {
    log("Page 'new' left")
  }
}
