package loom.charm.pages

import loom.charm.DataLayer
import nephtys.loom.charm.protocol.CharmProtocol._
import nephtys.loom.charm.protocol._
import org.querki.jquery._
import org.scalajs.dom
import org.widok._
import org.widok.bindings.Bootstrap.{Button, Input, Label, Style}
import org.widok.bindings.{Bootstrap, HTML}
import org.widok.html.{div, label, option, select}
import loom.charm.ReusableUIComponents._
import org.widok.bindings.HTML.Input.Checkbox

import scala.collection.mutable
import scala.util.Try

/**
  * Created by nephtys on 11/16/16.
  */
case class EditDetailsPage(dataLayer: DataLayer)() extends Page {
  private val power = Var[Option[Power]](None)
  //bind everything else to this, and compare later on in method
  private val powerType = power.map(_.map(_.readableType))
  private val isMyPower = power.zip(dataLayer.tokenservice.inMemoryEmail).map(a => a._1.map(_.owner).equals(a._2))


  private object state {
    val hasCategory: ReadChannel[Boolean] = power.map(_.exists(_.isInstanceOf[PowerWithCategory]))
    val hasTypesAndMins: ReadChannel[Boolean] = power.map(_.exists(_.isInstanceOf[PowerWithTypeAndMins]))
    val hasPrereqs: ReadChannel[Boolean] = power.map(_.exists(_.isInstanceOf[PowerWithPrereq]))
    val isSpell: ReadChannel[Boolean] = power.map(_.exists(_.isInstanceOf[Spell]))
    val isCharm: ReadChannel[Boolean] = power.map(_.exists(_.isInstanceOf[Charm]))
    val isRepurchase: ReadChannel[Boolean] = power.map(_.exists(_.isInstanceOf[Repurchase]))
    val isEvocation: ReadChannel[Boolean] = power.map(_.exists(_.isInstanceOf[Evocation]))
  }

  private def initializeWithPower(p: Power): Unit = {
    //generic
    editable.:=(false)
    toPublish := p.public
    toNewName := p.name
    toNewOwner := p.owner.email
    p.cost match {
      case Left(set) =>
        coststate.selectedCosts.set(set.toSeq.map(k => (k.copiedWithNumber(1).toString, k.number)))
        coststate.selectedOptionalCosts.clear()
        coststate.optionalCostChecked := false
        coststate.toNewCostIntField := 1.toString
        coststate.toNewCostSelected := coststate.possibleCostsStr.get.headOption
        coststate.toNewOptionalCostSelected := coststate.possibleCostsStr.get.headOption
        coststate.toNewOptionalCostIntField := 1.toString
      case Right((set, optionset)) =>
        coststate.selectedCosts.set(set.toSeq.map(k => (k.copiedWithNumber(1).toString, k.number)))
        coststate.selectedOptionalCosts.set(optionset.toSeq.map(k => (k.copiedWithNumber(1).toString, k.number)))
        coststate.optionalCostChecked := true
        coststate.toNewCostIntField := 1.toString
        coststate.toNewCostSelected := coststate.possibleCostsStr.get.headOption
        coststate.toNewOptionalCostSelected := coststate.possibleCostsStr.get.headOption
        coststate.toNewOptionalCostIntField := 1.toString
    }
    val durationmix: (String, Int, String) = p.duration match {
      case One(units) => ("One(something)", 0, units)
      case Hours(hours) => ("Hours(0)", hours, "")
      case Turns(turns) => ("Turns(0)", turns, "")
      case d: Duration => (d.toString, 0, "")
    }
    toNewDurationType.:=(Some(durationmix._1))
    toNewDurationIntStr.:=(durationmix._2.toString)
    toNewDurationString.:=(durationmix._3)
    toNewKeywords.set(p.keywords.map(_.title).toSeq)
    toNewDescription := p.description.description


    p match {
      case c: Charm => {
        //prerequisites
        prerequisitestate.typeOfPrerequisitesSelected := prerequisitestate.typeOfPrerequisiteSelectValues.get.tail.headOption
        prerequisitestate.toBeUnrelatedCharmsGeneralNumberStr := 1.toString
        prerequisitestate.toBeUnrelatedCharmsCategoryNumberStr := 1.toString
        prerequisitestate.toBeUnrelatedCharmsCategoryField := prerequisitestate.possibleCategories.get.headOption.map(_.str)
        prerequisitestate.toNewPrerequisites.set(c.prerequisiteCharms.toSeq)
        prerequisitestate.charmSearchFieldValue := ""
        prerequisitestate.recalcFilteredCharms()

        //mins
        c.mins match {
          case Left(essence) =>
            minstate.minAttributeSelected := true
            minstate.toNewMinAttribute := ""
            minstate.toNewMinAttributeDotsStr := 0.toString
            minstate.toNewMinEssenceDotsStr := essence.rating.toString
          case Right((essence, ability)) =>
            minstate.minAttributeSelected := true
            minstate.toNewMinAttribute := ability.name
            minstate.toNewMinAttributeDotsStr := ability.rating.toString
            minstate.toNewMinEssenceDotsStr := essence.rating.toString
        }

        //category
        toNewCategory := c.category.str

        //charmtype
        typestate.typeVar := Some(c.typ.toString)
      }
      case r: Repurchase => {
        //mins
        r.mins match {
          case Left(essence) =>
            minstate.minAttributeSelected := true
            minstate.toNewMinAttribute := ""
            minstate.toNewMinAttributeDotsStr := 0.toString
            minstate.toNewMinEssenceDotsStr := essence.rating.toString
          case Right((essence, ability)) =>
            minstate.minAttributeSelected := true
            minstate.toNewMinAttribute := ability.name
            minstate.toNewMinAttributeDotsStr := ability.rating.toString
            minstate.toNewMinEssenceDotsStr := essence.rating.toString
        }

        //charmtype
        typestate.typeVar := Some(r.typ.toString)
      }
      case s: Spell => {
        //circle:
        circleVar := Some(s.circle.toString)
      }
      case e: Evocation => {
        //prerequisites
        prerequisitestate.typeOfPrerequisitesSelected := prerequisitestate.typeOfPrerequisiteSelectValues.get.tail.headOption
        prerequisitestate.toBeUnrelatedCharmsGeneralNumberStr := 1.toString
        prerequisitestate.toBeUnrelatedCharmsCategoryNumberStr := 1.toString
        prerequisitestate.toBeUnrelatedCharmsCategoryField := prerequisitestate.possibleCategories.get.headOption.map(_.str)
        prerequisitestate.toNewPrerequisites.set(e.prerequisiteCharms.toSeq)
        prerequisitestate.charmSearchFieldValue := ""
        prerequisitestate.recalcFilteredCharms()

        //mins
        e.mins match {
          case Left(essence) =>
            minstate.minAttributeSelected := true
            minstate.toNewMinAttribute := ""
            minstate.toNewMinAttributeDotsStr := 0.toString
            minstate.toNewMinEssenceDotsStr := essence.rating.toString
          case Right((essence, ability)) =>
            minstate.minAttributeSelected := true
            minstate.toNewMinAttribute := ability.name
            minstate.toNewMinAttributeDotsStr := ability.rating.toString
            minstate.toNewMinEssenceDotsStr := essence.rating.toString
        }

        //category
        toNewCategory := e.category.str

        //charmtype
        typestate.typeVar := Some(e.typ.toString)
      }
    }
  }

  power.attach(o => o.foreach(initializeWithPower))

  private val editable = Var[Boolean](true)

  private val toPublish = Var[Boolean](false)
  private val toPublishString = toPublish.map(b => if (b) "Public (for all to see)" else "Private (only visible for me)")
  private val toPublishBtnString = toPublish.map(b => if (b) "Make Private" else "Make Public")

  private val toNewOwner = Var[String]("")
  private val toNewOwnerEmail = toNewOwner.map(s => Email(s)).cache

  private val toDelete = Var[Boolean](false)
  private val toDeleteText = toDelete.map(b => if (!b) "Delete permanently (cannot be restored)" else "You deleted this power, but change not committed! Press Cancel to abort permanent deletion.")


  private val toNewName = Var[String]("")

  private val toNewDescription = Var[String]("")
  private val toNewDescriptionBoxed = toNewDescription.map(s => Description(s)).cache

  private val toNewCategory = Var[String]("")
  private val toNewCategoryBoxed = toNewCategory.map(s => Category(s)).cache

  private object typestate {
  val possibleCharmTypes = Seq(Reflexive, Permanent, Supplemental, Simple)
    val possibleCharmTypesMap : Map[String, CharmType] = possibleCharmTypes.map(c => (c.toString, c)).toMap
  val possibleCharmTypesString = Buffer.from(possibleCharmTypes.map(_.toString))
  val typeVar: Var[Option[String]] = Var(Some(possibleCharmTypesString.get.head))
    def strToType(s : String) : CharmType = possibleCharmTypesMap.getOrElse(s, Simple)
  }

  private val possibleCircles = Seq(Terrestrial, Celestial, Solar)
  private val possibleCirclesMap: Map[String, SorceryCircle] = possibleCircles.map(a => (a.toString, a)).toMap
  private val possibleCirclesString = Buffer.from(possibleCircles).map(_.toString).buffer
  private val circleVar: Var[Option[String]] = Var(Some(possibleCirclesString.get.head))
  private val circleVarBoxed: ReadStateChannel[SorceryCircle] = circleVar.map(o => o.map(s => possibleCirclesMap(s)).getOrElse(Terrestrial)).cache

  private val toNewKeywords: Buffer[String] = Buffer("keyword A", "keyword B", "keyword C")
  private val toNewKeywordsBoxed: ReadBuffer[Keyword] = toNewKeywords.map(s => Keyword(s)).buffer
  private val addNewKeywordFieldVar: Var[String] = Var("")

  private object minstate {
    val toNewMinEssenceDotsStr: Var[String] = Var("1")
    val toNewMinAttributeDotsStr: Var[String] = Var("1")
    val toNewMinEssenceDots: ReadStateChannel[Byte] = toNewMinEssenceDotsStr.map(s => Try(Integer.parseInt(s)).toOption.getOrElse(-1).toByte).cache
    val toNewMinAttributeDots: ReadStateChannel[Byte] = toNewMinAttributeDotsStr.map(s => Try(Integer.parseInt(s)).toOption.getOrElse(-1).toByte).cache
    val minAttributeSelected: Var[Boolean] = Var(false)
    val toNewMinAttribute: Var[String] = Var("")
    val toNewMinimumBoxed: ReadStateChannel[Either[Essence, (Essence, Ability)]] = {
      minAttributeSelected.zip(toNewMinEssenceDots.zip(toNewMinAttributeDots.zip(toNewMinAttribute))).map[Either[Essence, (Essence, Ability)]](
        a => if (a._1) Left(Essence(a._2._1)) else Right((Essence(a._2._1), Ability(a._2._2._2, a._2._2._1)))
      ).cache
    }
  }


  private val possibleDurations: Seq[Duration] = Seq(Instant,
    InstantOrUntilEnded, UntilSunrise, OneCombat, OneScene, Permanent, Reflexive, UntilEnded, Indefinite,
    OneDay,
    Turns(0), One("something"), Hours(0))
  private val durationMap: Map[String, Duration] = possibleDurations.map(a => (a.toString, a)).toMap
  assert(possibleDurations.size == 13)
  private val possibleDurationsString = Buffer.from[String](possibleDurations.map(_.toString))
  private val toNewDurationType: Var[Option[String]] = Var(Some(possibleDurationsString.get.head))
  private val toNewDurationIntStr: Var[String] = Var("")
  private val toNewDurationString: Var[String] = Var("")
  private val toNewDurationInt = toNewDurationIntStr.map(s => Try(Integer.parseInt(s)).getOrElse(1)).cache
  private val toNewDurationBoxed: ReadStateChannel[Duration] = toNewDurationType.zip(toNewDurationInt.zip(toNewDurationString)).map[Duration](a => {
    if (a._1.getOrElse("N/A") == possibleDurations(10).toString) {
      Turns(a._2._1)
    } else if (a._1.getOrElse("N/A") == possibleDurations(11).toString) {
      One(a._2._2)
    } else if (a._1.getOrElse("N/A") == possibleDurations(12).toString) {
      Hours(a._2._1)
    } else {
      durationMap(a._1.getOrElse(possibleDurations.head.toString))
    }
  }).cache


  private object coststate {
     val possibleCosts: Seq[Int => Cost] = Seq(
      i => SorcerousMotes(i), i => Motes(i), i => Willpower(i), i => Initiative(i),
      i => Experience(i), i => BashingHealth(i), i => LethalHealth(i), i => AggravatedHealth(i)
    )
    assert(possibleCosts.size == 8)


     val buildCost: Map[String, Int => Cost] = possibleCosts.map(a => (a.apply(1).toString, a)).toMap

     def addNewCostFunc(i: Int, s: String): Unit = {
      selectedCosts.+=((s, i))
      toNewCostIntField.:=("1")
    }

     def addNewOptionalCostFunc(i: Int, s: String): Unit = {
      selectedOptionalCosts.+=((s, i))
      toNewOptionalCostIntField.:=("1")
    }

     val possibleCostsStr: ReadBuffer[String] = Buffer.from(possibleCosts.map(f => f.apply(1).toString))
     val toNewCostSelected: Var[Option[String]] = Var(possibleCostsStr.get.headOption)
     val toNewCostIntField: Var[String] = Var("5")
     val selectedCosts: Buffer[(String, Int)] = Buffer()
     val alreadySelectedCost: ReadBuffer[String] = selectedCosts.map(_._1).buffer
     val toNewOptionalCostSelected: Var[Option[String]] = Var(possibleCostsStr.get.headOption)
     val toNewOptionalCostIntField: Var[String] = Var("5")
     val selectedOptionalCosts: Buffer[(String, Int)] = Buffer()
     val alreadySelectedOptionalCost: ReadBuffer[String] = selectedCosts.map(_._1).buffer
     val optionalCostChecked: Var[Boolean] = Var(false)

  }

  private object prerequisitestate {
   val charmSearchFieldValue: Var[String] = Var("")
   val searchFieldInBuffer: Buffer[String] = Buffer("")

   def recalcFilteredCharms(): Unit = {
    println("Searchfield changed...")
    searchFieldInBuffer.clear()
    searchFieldInBuffer.+=(charmSearchFieldValue.get)
  }

   def categoryContains(p: Power, s: String): Boolean = {
    p match {
      case charm: Charm => charm.category.str.contains(s)
      case evo: Evocation => evo.category.str.contains(s)
      case _ => false
    }
  }

   val currentlyFilteredFirst5Charms: ReadBuffer[Power] = searchFieldInBuffer.flatMap(s => dataLayer.powers).filter(p => {
    val s = charmSearchFieldValue.get
    s.isEmpty || (p.description.description.contains(s)
      || p.name.contains(s)
      || p.keywords.exists(_.title.contains(s)) || p.uuid.id.contains(s)
      || categoryContains(p, s))
  }).buffer.take(5)
   val toNewPrerequisites: Buffer[Prerequisites] = Buffer()
   val possibleCategories: ReadBuffer[Category] = dataLayer.categories
   val toBeUnrelatedCharmsCategoryField: Var[Option[String]] = Var(possibleCategories.get.headOption.map(_.str))
   val toBeUnrelatedCharmsCategoryNumberStr: Var[String] = Var("1")
   val toBeUnrelatedCharmsGeneralNumberStr: Var[String] = Var("1")
   val prerequisiteSelectOptionCategory: String = "A number of Charms from one category"
   val prerequisiteSelectOptionGeneral: String = "A number of Charms in general"
   val prerequisiteSelectOptionSpecific: String = "A specific Charm from the list"
   val typeOfPrerequisiteSelectValues: ReadBuffer[String] = Buffer(prerequisiteSelectOptionSpecific, prerequisiteSelectOptionCategory, prerequisiteSelectOptionGeneral)
   val typeOfPrerequisitesSelected: Var[Option[String]] = Var(typeOfPrerequisiteSelectValues.get.tail.headOption)
   val prerequisiteSpecificFormVisible: ReadStateChannel[Boolean] = typeOfPrerequisitesSelected.map(_.contains(prerequisiteSelectOptionCategory)).cache
   val charmFilterVisible: ReadStateChannel[Boolean] = typeOfPrerequisitesSelected.map(_.contains(prerequisiteSelectOptionSpecific)).cache
   val prerequisiteGeneralFormVisible: ReadStateChannel[Boolean] = typeOfPrerequisitesSelected.map(_.contains(prerequisiteSelectOptionGeneral)).cache


}
  //:
  /*
  Publish (Boolean) DONE
  TransferOwnership (Email) DONE
  Delete (Button) DONE
  SetName (String) DONE
  SetDescription (String) DONE
  SetCategory (String) DONE
  SetCost two add /remove (uniform select + int), second checkbox dependand DONE
  SetMin (Essence int value + checkbox dependand attribute string + int value) DONE
  SetType (Select out of 4) DONE
  Keyword Add/Remove (new String) DONE
  SetDuration (Select, with special Int or special String in some cases) DONE
  Prerequisite Add/Remove (Select from existing or add unrelated /related number) DONE
  SetCicle (Select out of 3) DONE

   */




  protected def enableEntireFormRecursively(enable : Boolean) : Unit = {
    val t = $("#charmformid").find("*").attr("disabled", !enable)
  }
  editable.attach(b => enableEntireFormRecursively(b))

  def view(): Widget[_] =
    HTML.Section(
      HTML.Heading.Level1(power.map(o => o.map(p => "Editing " + p.name).getOrElse("N/A"))),
      Button("Edit").style(Style.Info).visible(editable.is(false)).onClick(f => editButtonPressed()),
      form(),
      Button("Cancel Changes").style(Style.Danger).show(editable.is(true)).onClick(f => cancelButtonPressed()),
      Button("Save Changes").style(Style.Success).show(editable.is(true)).onClick(f => saveButtonPressed()),
      Button("Return to Main").style(Style.Primary).show(editable.is(false)).onClick(f => cancelButtonPressed())

    )

  protected def form(): Widget[_] = {
    HTML.Container.Generic(
      div(
        label(toPublishString).attribute("for", "publishbtn"),
        Button(toPublishBtnString).style(Style.Info).attribute("class", "form-control").attribute("id", "publishbtn").onClick(f => makePublicBtnPressed())
      ).attribute("class", "form-group"),
      div(
        label("Transfering ownership to another owner").attribute("for", "newownerfield"),
        Input.Text().attribute("class", "form-control").placeholder("Email address of owner...").bind(toNewOwner).attribute("id", "newownerfield")
      ).attribute("class", "form-group"),
      div(
        label("Delete this entry permanently").attribute("for", "deletebtn"),
        Button(toDeleteText).style(Style.Danger).attribute("class", "form-control").enabled(toDelete.is(false)).attribute("id", "deletebtn").onClick(f => deleteBtnPressed())
      ).attribute("class", "form-group"),
      div(
        label(powerType.map(s => s"Name of ${s.getOrElse("N/A")}")).attribute("for", "newnamefield"),
        Input.Text().attribute("class", "form-control").placeholder("New name of power...").bind(toNewName).attribute("id", "newnamefield")
      ).attribute("class", "form-group"),
      div(
        label("Description").attribute("for", "newdescriptionfield"),
        Input.Textarea().attribute("class", "form-control").placeholder("New description of power...").bind(toNewDescription).attribute("id", "newdescriptionfield")
      ).attribute("class", "form-group"),
      div(
        label("Category (e.g. Attribute or Martial Arts Style)").attribute("for", "newcategoryfield"),
        Input.Text().attribute("class", "form-control").placeholder("New category of power...").bind(toNewCategory).attribute("id", "newcategoryfield")
      ).attribute("class", "form-group").show(state.hasCategory),
      div(
        div(intAndSelectCombiner(Var("Adding Normal Cost"), Var("Add"), coststate.toNewCostIntField, coststate.toNewCostSelected, coststate.possibleCostsStr, Some(coststate.alreadySelectedCost), Some(coststate.addNewCostFunc)))
        , div(
          Label("Currently costing"),
          HTML.List.Unordered(
            coststate.selectedCosts.map((a: (String, Int)) => HTML.List.Item(Button(a.toString() + " (tap to remove)").onClick(f => coststate.selectedCosts.remove((a._1, a._2)))))
          )),
          Bootstrap.Checkbox(
        HTML.Input.Checkbox().bind(coststate.optionalCostChecked),
        "Optional Cost"
      ),
        div(
          div(intAndSelectCombiner(Var("Adding Optional Cost to Power"), Var("Add"), coststate.toNewOptionalCostIntField, coststate.toNewOptionalCostSelected, coststate.possibleCostsStr, Some(coststate.alreadySelectedOptionalCost), Some(coststate.addNewOptionalCostFunc)))
          , div(
            Label("Currently costing optionally"),
            HTML.List.Unordered(
              coststate.selectedOptionalCosts.map((a: (String, Int)) => HTML.List.Item(Button(a.toString() + " (tap to remove)").onClick(f => coststate.selectedCosts.remove((a._1, a._2)))))
            ))
        ).show(coststate.optionalCostChecked)
      ),
      div(
        label("Minimum Requirements").forId("minimimumformfield"),
        div(
          div(
            label("Essence").forId("minessencefield"),
            Input.Number().attribute("class", "form-control").bind(minstate.toNewMinEssenceDotsStr).attribute("id", "minessencefield")
          ),
          div(
            Bootstrap.Checkbox(
              HTML.Input.Checkbox().bind(minstate.minAttributeSelected),
              "Ability required"
            ),
            div(
              Input.Text().placeholder("name of Ability").attribute("class", "form-control").bind(minstate.toNewMinAttribute).attribute("id", "minessencefield"),
              Input.Number().attribute("class", "form-control").bind(minstate.toNewMinAttributeDotsStr).attribute("id", "minessencefield")
            ).show(minstate.minAttributeSelected)
          )

        ) //.attribute("class", "form-control")
          .id("minimimumformfield")
      ).show(state.hasTypesAndMins) //.attribute("class", "form-group")
      ,
      div(
        label("Power Type").attribute("for", "newpowertypefield"),
        select().bind(typestate.possibleCharmTypesString, option, typestate.typeVar).attribute("class", "form-control").attribute("id", "newpowertypefield")
      ).attribute("class", "form-group").show(state.hasTypesAndMins),
      div(
        label("Keywords:"),
        div(div(
          manuallyTriggeredTextInputStr("Add Keyword", "some prompt text for new keywords", addNewKeywordFieldVar, Some(toNewKeywords), s => {
            toNewKeywords.+=(s)
            addNewKeywordFieldVar.:=("")
          })
        ),
          HTML.List.Unordered(
            toNewKeywords.map(k => {
              HTML.List.Item(Button(k + " (tap to delete)").style(Style.Default).onClick(f => removeKeyword(k)))
            })
          ))
        //Inline("Here be alreadyexisting keywords that can be deleted")
      ).attribute("class", "form-group"),
      div(
        label("Duration").attribute("for", "newdurationfield"),
        select().bind(possibleDurationsString, option, toNewDurationType).attribute("class", "form-control").attribute("id", "newdurationfield"),
        Input.Text().show(toNewDurationType.is(Some(possibleDurations(11).toString))).bind(toNewDurationString),
        Input.Number().show(toNewDurationType.map(o => o.exists(str => str.equals(possibleDurations(12).toString) || str.equals(possibleDurations(10).toString)))).bind(toNewDurationIntStr)
      ).attribute("class", "form-group"),
      prerequisiteForm().show(state.hasPrereqs),
      div(
        label("Sorcery Circle").attribute("for", "newcirclefield"),
        select().bind(possibleCirclesString, option, circleVar).attribute("class", "form-control").attribute("id", "newcirclefield")
      ).attribute("class", "form-group").show(state.isSpell)
    ).id("charmformid")
  }


  private val prerequisiteBtnText : ReadChannel[String] = this.prerequisitestate.charmFilterVisible.map(b => if(b) "Filter" else "Add")
  protected def prerequisiteBtnPressed() : Unit = {
    if (this.prerequisitestate.charmFilterVisible.get) {
      this.prerequisitestate.recalcFilteredCharms()
    } else if (this.prerequisitestate.prerequisiteSpecificFormVisible.get) {
      val p = UnrelatedCharmsOfCategory(Category(this.prerequisitestate.toBeUnrelatedCharmsCategoryField.get.getOrElse("N/A")), Try(Integer.parseInt(this.prerequisitestate.toBeUnrelatedCharmsCategoryNumberStr.get)).getOrElse(1) )
      this.prerequisitestate.toNewPrerequisites.+=(p)
    } else {
      //general prerequisite
      val p = UnrelatedCharms(Try(Integer.parseInt(this.prerequisitestate.toBeUnrelatedCharmsGeneralNumberStr.get)).getOrElse(1) )
      this.prerequisitestate.toNewPrerequisites.+=(p)
    }
  }

  protected def prerequisiteForm() : HTML.Container.Generic = {
    div(
      div(label(this.powerType.map(_.map(s => "Prerequisite for " + s).getOrElse("_blub_")))), //label
      div(Inline("Here be new Prerequisites control"),
        twoWidgetsAndAButtonInline(Var("Add new"), prerequisiteBtnText, prerequisiteBtnPressed, Var(true),
          select().bind(this.prerequisitestate.typeOfPrerequisiteSelectValues, option, this.prerequisitestate.typeOfPrerequisitesSelected).attribute("class", "form-control"),
          div(
           div(Input.Text().bind(this.prerequisitestate.charmSearchFieldValue).onKeyPress(e => {
             if(e.key == "Enter") this.prerequisitestate.recalcFilteredCharms()
           })).show(this.prerequisitestate.charmFilterVisible), //just a textfield to filter
            div(Input.Number().bind(this.prerequisitestate.toBeUnrelatedCharmsCategoryNumberStr),
              select().bind(this.prerequisitestate.possibleCategories.map(_.str), option,
                this.prerequisitestate.toBeUnrelatedCharmsCategoryField).attribute("class", "form-control")
            ).show(this.prerequisitestate.prerequisiteSpecificFormVisible), //N Charms of category C
          div(Input.Number().bind(this.prerequisitestate.toBeUnrelatedCharmsGeneralNumberStr)).show(this.prerequisitestate.prerequisiteGeneralFormVisible) //general N Charms
          )
        )
      ), //add new, as inline-control
      div(
        label("Select a charm from those:"),
        //filtered charms
        HTML.List.Unordered(
          this.prerequisitestate.currentlyFilteredFirst5Charms.map(k => {
            HTML.List.Item(Button(k.name + " (tap to add)").style(Style.Success).onClick(f => this.prerequisitestate.toNewPrerequisites.+=(PrerequisiteCharm(k.uuid))))
          })
        )
      ).show(this.prerequisitestate.charmFilterVisible),
      div(
        label("Existing Prerequisites for this Power:"),
        HTML.List.Unordered(
          this.prerequisitestate.toNewPrerequisites.map(k => {
            HTML.List.Item(Button(k + " (tap to delete)").style(Style.Default).onClick(f => this.prerequisitestate.toNewPrerequisites.remove(k)))
          })
        )
      ) //list or remove old, as list of buttons
    )
  }



  def makePublicBtnPressed(): Unit = {
    toPublish.:=(!toPublish.get)
  }

  def deleteBtnPressed(): Unit = {
    if (dom.window.confirm(s"Do you really want to permanently delete power ${power.get.map(_.name).getOrElse("N/A")}?")) {
      toDelete.:=(true)
    }
  }

  def removeKeyword(keyword: String): Unit = {
    if (dom.window.confirm(s"Do you really want to delete the keyword$keyword?")) {
      toNewKeywords.remove(keyword)
    }
  }


  private def combineCostTo : Either[Set[Cost], (Set[Cost], Set[Cost])] = {
    if(coststate.optionalCostChecked.get) {
      Right((coststate.selectedCosts.get.map(a => coststate.buildCost(a._1).apply(a._2)).toSet, coststate.selectedOptionalCosts.get.map(a => coststate.buildCost(a._1).apply(a._2)).toSet))
    } else {
      Left(coststate.selectedCosts.get.map(a => coststate.buildCost(a._1).apply(a._2)).toSet)
    }
  }

  def calculateCommands: Seq[Command] = {
    power.get.map(p => internalCalculateCommands(p,
      toPublish.get,
      toNewOwnerEmail.get,
      toDelete.get,
      toNewName.get,
      toNewDescriptionBoxed.get,
      toNewDurationBoxed.get,
      toNewKeywordsBoxed.get.toSet,
      combineCostTo
      )(circleVarBoxed.get)
    (typestate.typeVar.get.map(typestate.strToType).getOrElse(Simple), minstate.toNewMinimumBoxed.get)
    (toNewCategoryBoxed.get,
      prerequisitestate.toNewPrerequisites.get.toSet
    )).getOrElse(Seq.empty)
  }


  def internalCalculateCommands(oldPower: Power, newPublicState: Boolean, newEmail: Email,
                                delete: Boolean, newName: String,
                                newDescription: Description, newDuration: Duration,
                                toKeywords: Set[Keyword],
                                newCost : Either[Set[Cost], (Set[Cost], Set[Cost])])
                               (circle: SorceryCircle)
                               (
                                 charmType: CharmType,
                                 newMinimum: Either[Essence, (Essence, Ability)]
                               )
                               (
                                 newCategory: Category,
                                 newPrerequisites : Set[Prerequisites]
                               ) : Seq[Command] = {
    def id = oldPower.uuid
    def uuid = oldPower.uuid
    power.get.map[Seq[Command]](unchangedPower => {
      if(delete) {
        //in case of delete, only apply the deletion (everxthing else would fail anyway)
        Seq(Delete(oldPower.uuid))
      } else {
        val commands = mutable.Buffer.empty[Command]

        //generals:
        if(newPublicState != oldPower.public) {
          commands += Publish(id, newPublicState)
        }
        if(!newEmail.equals(oldPower.owner)) {
          commands += TransferOwnership(id, newEmail)
        }
        if(!newName.equals(oldPower.name) ) {
          commands += SetName(id, newName)
        }
        if(!newDescription.equals(oldPower.description)) {
          commands += SetDescription(id, newDescription.description)
        }
        if(!newDuration.equals(oldPower.duration)) {
          commands += SetDuration(id, newDuration)
        }
        if(!toKeywords.equals(oldPower.keywords)) {
          commands ++= oldPower.keywords.diff(toKeywords).map(k => RemoveKeyword(id, k.title))
          commands ++= toKeywords.diff(oldPower.keywords).map(k => AddKeyword(id, k.title))
        }
        if(!newCost.equals(oldPower.cost)) {
          commands += SetCost(id, newCost)
        }

        //spells:
        if(oldPower.isInstanceOf[Spell] && !circle.equals(oldPower.asInstanceOf[Spell].circle)) {
          commands += SetCircle(id, circle)
        }

        //non-spells:
        if(oldPower.isInstanceOf[PowerWithTypeAndMins] && !charmType.equals(oldPower.asInstanceOf[PowerWithTypeAndMins].typ)) {
          commands += SetType(id, charmType)
        }
        if(oldPower.isInstanceOf[PowerWithTypeAndMins] && !newMinimum.equals(oldPower.asInstanceOf[PowerWithTypeAndMins].mins)) {
          commands += SetMins(id, newMinimum)
        }

        //evocations and charms:
        if(oldPower.isInstanceOf[PowerWithCategory] && !newCategory.equals(oldPower.asInstanceOf[PowerWithCategory].category)) {
          commands += SetCircle(id, circle)
        }
        if(oldPower.isInstanceOf[PowerWithPrereq] && !newPrerequisites.equals(oldPower.asInstanceOf[PowerWithPrereq].prerequisiteCharms)) {
          commands ++= oldPower.asInstanceOf[PowerWithPrereq].prerequisiteCharms.diff(newPrerequisites).map(k => RemovePrerequisite(id, k))
          commands ++= newPrerequisites.diff(oldPower.asInstanceOf[PowerWithPrereq].prerequisiteCharms).map(k => AddPrerequisite(id, k))
        }

        commands.toSeq
      }
    }).getOrElse(Seq.empty[Command])
  }

  def cancelButtonPressed(): Unit = {
    Routes.main().go()
    power.:=(None)
  }

  def saveButtonPressed(): Unit = {
    dataLayer.enqeueCommands(calculateCommands)
    dom.window.alert("You changed the charm, going back to the main page now...")
    Routes.main().go()
  }

  def editButtonPressed(): Unit = {
    editable.:=(true)
  }

  val selectedUPID: Var[String] = Var[String]("")

  private def recalcPower(): Unit = {
    power := dataLayer.getPowerToID(UniquePowerIdentifier(selectedUPID.get))
  }

  dataLayer.powers.changes.attach(f => recalcPower())
  selectedUPID.attach(f => recalcPower())

  def ready(route: InstantiatedRoute) {
    selectedUPID := route.args("param")
    editable.:=(false)
  }
}
