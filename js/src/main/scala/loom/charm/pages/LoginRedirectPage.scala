package loom.charm.pages

import loom.charm.DataLayer
import org.scalajs.dom
import org.widok.{Channel, InstantiatedRoute, Page}
import org.widok.bindings.HTML

/**
  * Created by nephtys on 11/19/16.
  */
case class LoginRedirectPage(dataLayer: DataLayer)() extends Page {
  def view() = HTML.Heading.Level1("You logged in successfully, redirecting to main page now...")

  def ready(route: InstantiatedRoute) {
    val token : String  = route.args("token")
    if (token != null) {
      dataLayer.tokenservice.setToken(token)
    }
    val timeouthandle = dom.window.setTimeout(() => Routes.main().go(), 2000)
  }

}
