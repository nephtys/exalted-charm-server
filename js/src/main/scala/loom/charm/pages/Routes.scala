package loom.charm.pages

/**
  * Created by nephtys on 11/16/16.
  */
import loom.charm.DataLayer
import org.widok._

object Routes {
  val datalayer : DataLayer = new DataLayer()

  val main     = Route("/"           , MainTablePage(datalayer)    )
  val create    = Route("/new", CreateNewPage(datalayer))
  val details     = Route("/details/:param", EditDetailsPage(datalayer)    )
  val notFound = Route("/404"        , NotFoundPage)
  val loginRedirect = Route("/login/:token", LoginRedirectPage(datalayer) )

  val routes = Set(main, create, details, notFound,loginRedirect)
}
