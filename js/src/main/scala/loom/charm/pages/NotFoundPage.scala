package loom.charm.pages

import org.widok.Page

import org.scalajs.dom

import org.widok._
import org.widok.bindings.HTML

/**
  * Created by nephtys on 11/16/16.
  */
case class NotFoundPage() extends Page{
  def view() = HTML.Heading.Level1("Page not found")

  def ready(route: InstantiatedRoute) {
    val timeouthandle = dom.window.setTimeout(() => Routes.main().go(), 2000)
  }
}
