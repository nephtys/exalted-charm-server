package loom.charm.MiddleFSM

import loom.charm.Services.{AlternateQueueService, HttpService, IndexedDBService}
import nephtys.loom.charm.protocol.CharmProtocol.Command

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by nephtys on 12/1/16.
  */
class AlternateStateManager(implicit httpService: HttpService,
                            indexedDBService: IndexedDBService,
                            queueService: AlternateQueueService) {


  val communicationManager : CommunicationManager = new CommunicationManager()

  def normalTimerTrigger() : Unit = {
    //get unsent commands
   val commands = queueService.getUnsentCommands
    //if nonempty, send them
    //after return, call rest
    val f = if(commands.nonEmpty) {
      communicationManager.postCommandsRemotely(commands).flatMap(b => {
        (if (b) {
          queueService.iHaveSentCommandsSuccessful(commands)
        } else {
          println(s"Post Remotely Failed for commands: $commands")
          Future.successful(b)
        }).flatMap(t => {
          communicationManager.setRemotePowersLocally()
        })
      })
    } else {
      communicationManager.setRemotePowersLocally()
    }
  }

  def enqueueCommand(commands : Seq[Command]) : Unit = {
    //filter commands
    val filtered = indexedDBService.filterCommands(commands)
    //apply them locally
    indexedDBService.commitCommandsLocally(filtered)
    //write them down
    queueService.addNewUnsentCommands(filtered)
    //cause normalTrigger
    normalTimerTrigger()
  }

  def tokenChanged() : Unit = normalTimerTrigger()

  tokenChanged() //init
}
