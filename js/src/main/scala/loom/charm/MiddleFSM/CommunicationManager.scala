package loom.charm.MiddleFSM

import loom.charm.Services.{HttpService, IndexedDBService}
import nephtys.loom.charm.protocol.CharmProtocol.Serialization.FailableEventList
import nephtys.loom.charm.protocol.CharmProtocol.{Command, Event}
import nephtys.loom.charm.protocol.Power
import nephtys.loom.charm.server.Failable
import org.widok.{ReadStateChannel, Var}
import upickle.default._

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by nephtys on 12/1/16.
  */
class CommunicationManager(implicit httpService: HttpService, indexedDBService: IndexedDBService) {

  val onlineInternal : Var[Boolean] = Var(true)




  val online : ReadStateChannel[Boolean] = onlineInternal.distinct.cache

  online.attach(b => println(s"online state changed to $b"))

  def postCommandsRemotely(commands : Seq[Command]) : Future[Boolean] = {
    val promise = Promise[Boolean]()
      httpService.authenticatedPost(httpService.charmURL, write(commands)).onComplete {
        case Success(t) => {
          println("Success while posting")
          if (t.status == 200) {
            if(!online.get) onlineInternal := true
            //parse Failables and show errors
            Try(read[FailableEventList](t.responseText)).foreach(_.results.foreach {
              case Left(event) => println(s"Remote confirmed command with event $event")
              case Right(error) => println("A command was returned as illegal by remote with error message: " + error)
            })
            promise.trySuccess(true)
          } else if (t.status == 304) {
            if(!online.get) onlineInternal := true
            println("HTTP Result is cached")
            promise.trySuccess(false)
          } else if(t.status == 401) {
            if(!online.get) onlineInternal := true
            println("Auth Error on HTTP!")
            promise.trySuccess(false)
          } else {
            println(s"Unrecognized HTTP status: ${t.status}")
            promise.trySuccess(false)
          }
        }
        case Failure(e) => {
          onlineInternal  := false
          promise.tryFailure(e)
        }
      }
    promise.future
  }

  def setRemotePowersLocally() : Future[Boolean] = {
    val promise = Promise[Boolean]()
    httpService.authenticatedGet(httpService.charmURL).onComplete {
      case Success(t) => {
        println(s"Success while getting, with headers = ${t.getAllResponseHeaders()}")
        if (t.status == 200) {
          val newetag = t.getResponseHeader("ETag")
          if(newetag != null && newetag.nonEmpty) {
            httpService.ETag := newetag
          }
          if(!online.get) onlineInternal := true
          //parse and write locally
          indexedDBService.setAllFromRemote(read[Seq[Power]](t.responseText))
          promise.trySuccess(true)
        } else if (t.status == 304) {
          if(!online.get) onlineInternal := true
          println("HTTP Result is cached")
          promise.trySuccess(false)
        } else if(t.status == 401) {
          if(!online.get) onlineInternal := true
          println("Auth Error on HTTP!")
          promise.trySuccess(false)
        } else {
          println(s"Unrecognized HTTP status: ${t.status}")
          promise.trySuccess(false)
        }
      }
      case Failure(e) => {
        onlineInternal  := false
        promise.tryFailure(e)
      }
    }
    promise.future
  }
}
