package loom.charm

import java.util.UUID

import loom.charm.MiddleFSM.AlternateStateManager
import loom.charm.Services._
import nephtys.loom.charm.protocol.CharmProtocol.Command
import nephtys.loom.charm.protocol._
import org.widok.{Buffer, ReadBuffer, ReadChannel, Var}
import rxscalajs.Observable

import scala.util.{Failure, Success, Try}

/**
  * Created by nephtys on 11/18/16.
  */
class DataLayer {
  implicit val tokenservice = new TokenService()
  implicit val idbservice = new IndexedDBService()
  implicit val queueservice = new AlternateQueueService()
  implicit val httpservice = new HttpService()


  val stateManager : AlternateStateManager = new AlternateStateManager()




  def logoutButtonPressed(): Unit = {
    org.scalajs.dom.window.localStorage.removeItem(tokenservice.localStorageTokenKey)
    //recheckLocalStorageForTokens()
  }

  def powers : ReadBuffer[Power] = idbservice.powers
  val categories : Buffer[Category] = Buffer.from(powers.get.filter(_.isInstanceOf[PowerWithCategory]).map(_.asInstanceOf[PowerWithCategory].category).distinct)
  powers.changes.attach(d => categories.set(powers.get.filter(_.isInstanceOf[PowerWithCategory]).map(_.asInstanceOf[PowerWithCategory].category).distinct))

  def getPowerToID(upid : UniquePowerIdentifier) : Option[Power] = {
    this.idbservice.getPowerToID(upid)
  }

  def randomID() : String = UUID.randomUUID().toString

  val interval: Observable[Int] = Observable.interval(15000)


  //interval.subscribe(l => println("ping #"+l.toString))

  interval.subscribe(l => stateManager.normalTimerTrigger() )

  interval.subscribe(l => if (tokenservice.tokenIsExpiring.exists(l => l < System.currentTimeMillis())) {
    if (org.scalajs.dom.window.confirm("Your Google token is expired. Do you want to login anew now (page will reload and data may be lost)?")) {
      org.scalajs.dom.window.location.href = "/login"
    }
  })

  interval.subscribe(l => tokenservice.loadTokenFresh())


  def enqeueCommand(command : Command) : Unit =  {
    println(s"enqueued solo following command: $command")
    enqeueCommands(Seq(command))
  }

  def enqeueCommands(command : Seq[Command]) : Unit =  {
    println(s"enqued following commands: $command")
    stateManager.enqueueCommand(command)
  }

}
