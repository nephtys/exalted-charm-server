package loom.charm

import nephtys.loom.charm.protocol._

/**
  * Created by nephtys on 11/17/16.
  */
object Mockdata {

  val sender = Email("christopher.kaag@gmail.com")

  val id0 = "id0"
  val id1 = "id1"
  val id2 = "id2"
  val id3 = "id3"
  val id4 = "id4"


  val internal = Seq(
    Spell(UniquePowerIdentifier(id0), Terrestrial, "Frog Creation", Description("Creates frogs from " +
      "thin air"), Set.empty,
      Left
      (Set.empty), Instant, owner = sender, public = true),
    Charm(UniquePowerIdentifier(id1), "Wise Eye", Description("Let's you see far"), Category("Awareness"), Left(Set
      .empty), Left
    (Essence(1)), Reflexive, Set.empty, OneScene, Set.empty , owner = sender, public = true),
    Charm(UniquePowerIdentifier(id2), "Heavenly Guardian Defense", Description("Parry everything"),
      Category("Melee"), Left(Set.empty), Left
      (Essence(1)), Supplemental, Set.empty, Instant, Set.empty , owner = sender, public = true),
    Evocation(UniquePowerIdentifier(id3), Set.empty, Left(Set.empty), Instant, Category("Mighty Bow"), Reflexive, Left(Essence(2)
    ), "Unique Bull", Description("Let's the powerbow emit really strong light"), Set.empty, owner = sender, public = true))

  val mockdata : Seq[Power] = internal.+:(
    Repurchase(UniquePowerIdentifier(id4), Left(Set.empty), Left(Essence(2)), Reflexive, internal(1)
      .asInstanceOf[Charm], owner = sender, public = true)
  )
}
