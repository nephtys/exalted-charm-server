package loom.charm

import loom.charm.pages.Routes
import org.widok._
import org.widok.bindings.HTML

object Main extends RoutingApplication(
  Routes.routes
  , Routes.notFound
)