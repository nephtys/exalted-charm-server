package loom.charm

import java.util.concurrent.atomic.AtomicBoolean

import scala.collection.mutable
import scala.concurrent.Future

/**
  * Created by nephtys on 11/16/16.
  */
object Synchronize {

  private val blocked = new AtomicBoolean(false)

  private val queue : mutable.Queue[() => Unit] = mutable.Queue()

  def enqueueOperation(op : () => Unit) : Unit = {
    queue.enqueue(op)
    continueNext()
  }
  def continueNext() : Unit = {
    if (!blocked.get()) {
      blocked.set(true)
      if(queue.nonEmpty) {
        queue.dequeue.apply()
      }
      blocked.set(false)
    } else {
      import scala.concurrent.ExecutionContext.Implicits.global
      val f = Future { //wake me up when september ends (busy waiting using event loop)
        continueNext()
      }
    }
  }
}
