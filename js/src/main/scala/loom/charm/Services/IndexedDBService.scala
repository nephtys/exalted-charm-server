package loom.charm.Services

import loom.charm.Mockdata
import nephtys.loom.charm.protocol.CharmProtocol.Command
import nephtys.loom.charm.protocol.{CharmProtocol, Email, Power, UniquePowerIdentifier}
import org.scalajs.dom
import org.scalajs.dom.raw._
import org.widok.{Buffer, ReadBuffer}
import upickle.default._

import scala.collection.mutable
import scala.concurrent.Future
import scala.scalajs.js
import scala.scalajs.js._
import scala.scalajs.js.JSON
import scala.scalajs.js.annotation.JSExport
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by nephtys on 11/21/16.
  */
class IndexedDBService()(implicit tokenService : TokenService) {

  private implicit val config = IndexedDBService.powerConfig
  private def email = tokenService.myEmail.getOrElse(Email(""))

  //when starting, load data from database - if still empty, set mockdata

  private var aggregate: Map[UniquePowerIdentifier, Power] = Map.empty[UniquePowerIdentifier, Power]

  def getPowerToID(upid : UniquePowerIdentifier) : Option[Power] = {
      aggregate.get(upid)
  }


  def filterCommands(commands : Seq[Command]) : Seq[Command] = {
    var copy = aggregate
    commands.filter(command => {
      val t = command.validate(email, copy)
      t.foreach(event => copy = event.commit(aggregate))
      t.isSuccess
    })
  }

  private val internalPowers : Buffer[Power] = Buffer[Power]()
  val powers: ReadBuffer[Power] = internalPowers.buffer

    private def fillPowersFromDB() : Unit = {
      IndexedDBService.readDB[Power]() onComplete {
        case Success(list) => {
          println("=====================")
          println("Found following entries in database during bootstrap:")
          list.foreach(println)
          println("=====================")
          aggregate = list.groupBy(_.uuid).mapValues(_.head)
          internalPowers.++=(list)
        }
        case Failure(e) => println(s"readDB failed with $e")
      }
    }

  fillPowersFromDB()

  def commitCommandsLocally(seq: Seq[Command]) : Seq[Try[CharmProtocol.Event]] =    {
    //set in-memory copy and indexeddb content
    println(s"Trying to apply locally: $seq")
    val events = seq.map(_.validate(tokenService.myEmail.getOrElse(Email("")), aggregate))
    println(s"Verify Results : $events")
    var changed = Set.empty[UniquePowerIdentifier]
    events.foreach {
      case Success(event) => {
        println(s"Applying event $event locally")
        aggregate = event.commit(aggregate)
        changed = changed.+(event.id)
        internalPowers.set(aggregate.values.toSeq)
      }
      case Failure(e) => println(s"Warning: Command failed with error $e")
    }
    if (changed.nonEmpty) {
      storeAggregateInIndexedDB(aggregate.values.toSeq)
      internalPowers.update(p => if (changed.contains(p.uuid)) {
        aggregate(p.uuid)
      } else {
        p
      })
    }
    events
  }

  protected def storeAggregateInIndexedDB(seq : Seq[Power]) : Future[Boolean] = IndexedDBService.setDBToAll(seq)

  def setAllFromRemote(seq: Seq[Power]): Future[Boolean] = {
    internalPowers.set(seq)
    IndexedDBService.setDBToAll[Power](seq)
  }

  val debug = false
  if(debug) {
  setAllFromRemote(Mockdata.mockdata) onComplete  {
    case Success(true) => {
      println("setRemote successfull with true")
      IndexedDBService.readDB[Power]() onComplete {
        case Success(list) => {
          println("=====================")
          println("Found following entries in database:")
          list.foreach(println)
          println("=====================")
        }
        case Failure(e) => println(s"readDB failed with $e")
      }
    }
    case Success(false) => println("setRemote successfull with true")
    case Failure(e) => println(s"setRemote failed with $e")
  }}


}

object IndexedDBService {

  val database_name = "MyTestDatabaseForPowers"
  val database_version = 1

  def creatingIndexForId[T](idb : IDBDatabase, config : ObjectStoreConfig[T]) : Unit = {
    val store = idb.createObjectStore(config.objectStoreName)
  }

  val powerConfig : ObjectStoreConfig[Power] = ObjectStoreConfig[Power]("PowerObjectStore",
    database_name, t => t.uuid.id, creatingIndexForId[Power])


  case class ObjectStoreConfig[T](objectStoreName: String, dbName: String,
                                  idMapping : T => String, firsttimeopen: (IDBDatabase, ObjectStoreConfig[T]) => Unit
                                  = (e : IDBDatabase, t : ObjectStoreConfig[T]) => println("WARNING: DB opened with default handler")
                                 )


  def delete[T](ids : Set[String])(implicit reader: Reader[T], config: ObjectStoreConfig[T]) : Future[Boolean] = {
    val promise : scala.concurrent.Promise[Boolean] = scala.concurrent.Promise[Boolean]()

    def db = dom.window.indexedDB

    val open = db.open(config.dbName, IndexedDBService.database_version)

    // Create the schema
    open.onupgradeneeded = { (event: Event) => {
      val db2 = open.result.asInstanceOf[IDBDatabase]
      config.firsttimeopen.apply(db2, config)
    }
    }

    open.onsuccess = (event: Event) => {
      // Start a new transaction
      val db = open.result.asInstanceOf[IDBDatabase]

      val tx = db.transaction(config.objectStoreName, "readwrite")

      val store = tx.objectStore(config.objectStoreName)

      ids.toSeq.foreach(a => store.delete(a))

      // Close the db when the transaction is done
      tx.oncomplete = (event: Event) => {
        db.close()
        promise.success(true)
      }
      tx.onerror = (error : Any) => {
        db.close()
        promise.failure(new Exception(error.toString))
      }
    }
    promise.future
  }

  def add[T](elements : Map[String, T])(implicit writer: Writer[T], config: ObjectStoreConfig[T]) : Future[Boolean] = {
    val promise : scala.concurrent.Promise[Boolean] = scala.concurrent.Promise[Boolean]()

    def db = dom.window.indexedDB

    val open = db.open(config.dbName, IndexedDBService.database_version)

    // Create the schema
    open.onupgradeneeded = { (event: Event) => {
      val db2 = open.result.asInstanceOf[IDBDatabase]
      config.firsttimeopen.apply(db2, config)
    }
    }

    open.onsuccess = (event: Event) => {
      // Start a new transaction
      val db = open.result.asInstanceOf[IDBDatabase]

      val tx = db.transaction(config.objectStoreName, "readwrite")

      val store = tx.objectStore(config.objectStoreName)

      elements.toSeq.foreach(a => store.put(write(a._2), a._1))

      // Close the db when the transaction is done
      tx.oncomplete = (event: Event) => {
        db.close()
        promise.success(true)
      }
      tx.onerror = (error : Any) => {
        db.close()
        promise.failure(new Exception(error.toString))
      }
    }
    promise.future
  }


  def readDB[T]()(implicit reader: Reader[T], config: ObjectStoreConfig[T]): Future[List[T]] = {

    val promise : scala.concurrent.Promise[List[T]] = scala.concurrent.Promise[List[T]]()

    def db = dom.window.indexedDB

    val open = db.open(config.dbName, IndexedDBService.database_version)

    // Create the schema
    open.onupgradeneeded = { (event: Event) => {
      val db2 = open.result.asInstanceOf[IDBDatabase]
      config.firsttimeopen.apply(db2, config)
    }
    }

    open.onsuccess = (event: Event) => {
      // Start a new transaction
      val db = open.result.asInstanceOf[IDBDatabase]

      val tx = db.transaction(config.objectStoreName, "readonly")

      val keysBuffer = mutable.Buffer.empty[String]
      val valueBuffer = mutable.Buffer.empty[T]

      val store = tx.objectStore(config.objectStoreName)

      val cursorRequest = store.openCursor()
      cursorRequest.onerror = (error: Any) => {
        println("Error reading from IndexedDB : " + error)
      }


      cursorRequest.onsuccess = (event: Event) => {
        //println("success triggered")
        val cursor = cursorRequest.result.asInstanceOf[IDBCursor]
        if (cursor != null) {
          println("Found entry with key: " + cursor.key)
          keysBuffer.+=(cursor.key.asInstanceOf[String])
          cursor.continue()
        } else {
          keysBuffer.foreach(key => {
            val getValue = store.get(key)
            getValue.onsuccess = (event : Event) => {
              if (getValue.result != null && getValue.result.toString.isInstanceOf[String]) {
                val json : String = getValue.result.toString
                val t : T = read[T](json)
                valueBuffer.+=(t)
              }
            }
          })
        }
      }

      // Close the db when the transaction is done
      tx.oncomplete = (event: Event) => {
        db.close()
        promise.success(valueBuffer.toList)
      }
      tx.onerror = (error : Any) => {
        db.close()
        promise.failure(new Exception(error.toString))
      }
    }
    promise.future
  }

  def setDBToAll[T](seq: Seq[T])(implicit writer: Writer[T], config: ObjectStoreConfig[T]): Future[Boolean] = {

    val promise : scala.concurrent.Promise[Boolean] = scala.concurrent.Promise[Boolean]()

    def db = dom.window.indexedDB

    val open = db.open(config.dbName, IndexedDBService.database_version)

    // Create the schema
    open.onupgradeneeded = { (event: Event) => {
      val db2 = open.result.asInstanceOf[IDBDatabase]
      config.firsttimeopen.apply(db2, config)
    }
    }

    open.onsuccess = (event: Event) => {
      // Start a new transaction
      val db = open.result.asInstanceOf[IDBDatabase]

      val tx = db.transaction(config.objectStoreName, "readwrite")

      val store = tx.objectStore(config.objectStoreName)

      val cursorRequest = store.openCursor()
      cursorRequest.onerror = (error: Any) => {
        println("Error reading from IndexedDB : " + error)
      }


      cursorRequest.onsuccess = (event: Event) => {
        //println("success triggered")
        val cursor = cursorRequest.result.asInstanceOf[IDBCursor]
        if (cursor != null) {
          println("Deleting entry with key: " + cursor.key)
          cursor.delete()
          cursor.continue()
        } else {
          seq.foreach(t => store.put(write(t), config.idMapping.apply(t)))
        }
      }

      // Close the db when the transaction is done
      tx.oncomplete = (event: Event) => {
        db.close()
        promise.success(true)
      }
      tx.onerror = (error : Any) => {
        db.close()
        promise.failure(new Exception(error.toString))
      }
    }
    promise.future
  }
}