package loom.charm.Services

import java.util.concurrent.atomic.AtomicLong

import loom.charm.Services.AlternateQueueService.CommandContainer
import loom.charm.Services.IndexedDBService.ObjectStoreConfig
import nephtys.loom.charm.protocol.CharmProtocol.Command

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by nephtys on 12/1/16.
  */
class AlternateQueueService {

  private implicit val config = ObjectStoreConfig[Command]("QueueServiceObjectStore", "MyCommandDatabase", _.hashCode().toString, IndexedDBService.creatingIndexForId)
  println(s"QueueService is using config = $config")

  private val inMemoryCommandList : mutable.Buffer[Command] = mutable.Buffer()
  //initialization:
  IndexedDBService.readDB[Command]().foreach(list => inMemoryCommandList ++= list)

  def getUnsentCommands : List[Command] = {
    inMemoryCommandList.toList  //in-memory
  }
  def iHaveSentCommandsSuccessful(commands : Seq[Command]) : Future[Boolean] = {
    //causes writes and only returns after writes
    val done : Set[Command] = inMemoryCommandList.toSet.union(commands.toSet)
    inMemoryCommandList.clear()
    inMemoryCommandList ++= inMemoryCommandList.toSet.diff(commands.toSet)
    IndexedDBService.delete[Command](done.map(config.idMapping)).map(e => true)
  }
  def addNewUnsentCommands(commands : Seq[Command]) : Unit = {
    //causes persistent writes, but done in a fire-and-forget manner
    inMemoryCommandList ++= commands
    val f = IndexedDBService.add(commands.map(p => (p.hashCode().toString, p)).toMap)
  }


}

object AlternateQueueService {
  private lazy val idgen = new AtomicLong(System.currentTimeMillis())
  def generateUniqueAscendingID() : Long = idgen.incrementAndGet()
  final case class CommandContainer(ascendingId : Long, command : Command, queueA : Boolean)
}
