package loom.charm.Services

import java.util.Date

import loom.charm.OpenIDToken
import loom.charm.OpenIDToken.IdentityToken
import nephtys.loom.charm.protocol.Email
import org.widok.{ReadChannel, ReadPartialChannel, ReadStateChannel, Var}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

/**
  * Created by nephtys on 11/21/16.
  */
class TokenService {

  def currentTokenWithBearerKeyword : String = inMemoryTokenWithBearer.get
  def currentTokenWithoutBearerKeyword : String = inMemoryToken.get.getOrElse("N/A")


  def isSetToken : Boolean = currentTokenWithoutBearerKeyword.nonEmpty

  def unsetToken() : Unit = {
    setToken("")

  }



  def setToken(token: String) : Unit = {
    if (token.isEmpty) {
      org.scalajs.dom.window.localStorage.removeItem(localStorageTokenKey)
      inMemoryToken := None
    } else {
    org.scalajs.dom.window.localStorage.setItem(localStorageTokenKey, token)
      inMemoryToken := Some(token).filter(_.nonEmpty)
    }
  }

  def extractEmailFromToken(token : Option[String]) : Try[IdentityToken] = {
    val t = Try{
      OpenIDToken.base64EncodedTokenWithBearerHeader(token.get)
    }
    println(s"Result of extract method: $t")
    t

  }


  val localStorageTokenKey : String = "authentication_token"

  val inMemoryToken : Var[Option[String]] = Var(Some(org.scalajs.dom.window.localStorage.getItem(localStorageTokenKey))) //without "Bearer " in front

  val inMemoryEmail : ReadStateChannel[Option[Email]] = inMemoryToken.map[Option[Email]](token => extractEmailFromToken(token).toOption.map(s => Email(s.email))).cache

  inMemoryToken.attach(s => println(s"Token changed to $s"))
  inMemoryEmail.attach(s => println(s"Email changed to $s"))

  val inMemoryTokenExpiring: ReadStateChannel[Long] = inMemoryToken.map(token => extractEmailFromToken(token).map[Long](_.exp_long * 1000.toLong).getOrElse(0.toLong)).cache

  inMemoryTokenExpiring.attach(s => println(s"Token expiring at ${new Date(s)}"))

  def tokenIsExpiring : Option[Long] = Some(inMemoryTokenExpiring.get).filter(_ > 0)

  val inMemoryTokenWithBearer : ReadStateChannel[String] = inMemoryToken.map(s => "Bearer " + s.getOrElse("N/A")).cache

  def myEmail : Option[Email] = inMemoryEmail.get

  /**
    * without Bearer keyword
    */
  def loadTokenFresh() : Unit = {
    //println("loading token afresh")
    val perstBearer : String = org.scalajs.dom.window.localStorage.getItem(localStorageTokenKey)
    if (perstBearer != null && !inMemoryToken.get.contains(perstBearer)) {
      inMemoryToken.:=(Some(perstBearer))
    } else if(perstBearer == null) {
      inMemoryToken.:=(None)
    }
  }


  private def init() = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val f = /*Future*/ {
        loadTokenFresh()
      }
  }

  init()
}
