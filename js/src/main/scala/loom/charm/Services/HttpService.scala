package loom.charm.Services

import java.net.URI

import nephtys.loom.charm.protocol.CharmProtocol.{Command, Event}
import nephtys.loom.charm.protocol.{Email, Power}
import nephtys.loom.charm.server.Failable
import org.scalajs.dom.XMLHttpRequest
import org.scalajs.dom.ext.Ajax
import org.widok.Var
import upickle.default._

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by nephtys on 11/21/16.
  */
class HttpService(implicit tokenService: TokenService) {
  val loginURL = "/login"

  val ETag : Var[String] = Var("-1")
  ETag.attach(s => println(s"ETag changed to $s"))

  val headerName : String = "Authorization"
  def headerValue : String = tokenService.inMemoryTokenWithBearer.get
  val timeoutMs : Int = 5000

  val modifiedETagHeader : String = "If-None-Match"

  def headers : Map[String, String] = Map(headerName -> headerValue, modifiedETagHeader -> ETag.get) + CSRF.xforwardheader + CSRF.xrequestheader


  Future{
    println("Headers:")
    println(headers)
  }

  def authenticatedGet(url : String): Future[XMLHttpRequest] = {
    println(s"getting with header = $headerValue")
    Ajax.get(url, headers = this.headers, timeout = timeoutMs)
  }
  def authenticatedPost(url : String, json : String): Future[XMLHttpRequest] = {
    Ajax.post(url, json, timeout = timeoutMs, headers = this.headers)
  }

  object CSRF {

    def host : String = org.scalajs.dom.window.location.host
    def protocol : String = org.scalajs.dom.window.location.protocol
    def calculateCurrentOrigin : String = protocol +"//"+host


    protected def XForwardedHostHeader = """X-Forwarded-Host"""

    protected def OriginHeader = """Origin""" //implicitly filled by browser

    protected def XRequestedWithHeader = """X-Requested-With"""

    protected def XRequestedWithValue = """XMLHttpRequest"""

    def xforwardheader : (String, String) = XForwardedHostHeader -> calculateCurrentOrigin
    def xrequestheader  : (String, String) = XRequestedWithHeader -> XRequestedWithValue
  }

  //private val loginURL = "/login"
  val verifierURL = "/verify"
  val charmURL = "/charms"

  //def loginEndpoint : String = loginURL

  //error codes:
  //200 => new result
  //304 => same result as in cache
  //401 => token invalid
  //else => illegal return


  /*
    *
    *
    * .onComplete {
      case Success(s) => {
        if (s.status == 200) {

        } else if (s.status == 304) {

        } else if (s.status == 401) {

        } else {
            throw new IllegalStateException()
        }
      }
      case Failure(e) => {

      }
    }
    *
    *
    *
    */

  def verify() : Future[Boolean] = {
    authenticatedGet(verifierURL).map(s => read[Email](s.responseText).email.nonEmpty)
  }
  def charms() : Future[List[Power]] = {
    authenticatedGet(charmURL).map(s => read[List[Power]](s.responseText))
  }

  def postQueue(queueContent : Seq[Command]) : Future[List[Try[Event]]] = {
    authenticatedPost(charmURL, write(queueContent)).map(s => read[List[Failable[Event]]](s.responseText).map(_.asTry))
  }
}
