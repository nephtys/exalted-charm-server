package loom.charm

import java.util.UUID

import org.widok.bindings.Bootstrap.{Button, Input, Label, Style}
import org.widok.bindings.HTML
import org.widok.html.{div, span}
import org.widok.{Buffer, ReadBuffer, ReadChannel, ReadStateChannel, Var, Widget}
import org.widok._
import org.widok.bindings.Bootstrap.{Button, Input, Style}
import org.widok.bindings.{Bootstrap, HTML}
import org.widok.html.{div, label, option, select}

import scala.util.Try

/**
  * Created by nephtys on 11/27/16.
  */
object ReusableUIComponents {
  def manuallyTriggeredTextInputStr(buttonText : String,
                                 promptText : String,
                                    textFieldContent : Var[String],
                                 containsCheck : Option[ReadBuffer[String]],
                                 doWithResult : String => Unit
                                ) : Widget[_] = {
    manuallyTriggeredTextInput(Var(buttonText), Var(promptText), textFieldContent, containsCheck, doWithResult)
  }
  def manuallyTriggeredTextInputSimple(buttonText : String,
                                 doWithResult : String => Unit
                                ) : Widget[_] = {
    manuallyTriggeredTextInputStr(buttonText, "", Var(""), None, doWithResult)
  }
  def manuallyTriggeredTextInput(buttonText : ReadChannel[String],
                                 promptText : ReadChannel[String],
                                 textFieldContent : Var[String],
                                 containsCheck : Option[ReadBuffer[String]],
                                 doWithResult : String => Unit
                                ) : Widget[_] = {

    val allowed : ReadStateChannel[Boolean] = containsCheck.map(checker => {
      textFieldContent.map(s => s.nonEmpty && !checker.get.contains(s)).cache
    }).getOrElse(textFieldContent.map(s => s.nonEmpty).cache)

    div(
      div(
        div(
          Input.Text().attribute("class", "form-control").placeholder(promptText).bind(textFieldContent).onKeyPress(e => {
            if(e.key == "Enter" && allowed.get) doWithResult.apply(textFieldContent.get)
          }) ,
          span(
            Button(buttonText).style(Style.Default).onClick(f => doWithResult.apply(textFieldContent.get)).enabled(allowed)
          ).attribute("class", "input-group-btn")
        ).attribute("class", "input-group")
      ).attribute("class", "col-lg-6")
    ).attribute("class", "row")
  }



  def intAndSelectCombiner(
                            labelText : ReadChannel[String],
                            buttonText : ReadChannel[String],
                            intFieldStr : Var[String],
                           selectedField : Var[Option[String]],
                           possibleOptions : ReadBuffer[String],
                           containsCheck : Option[ReadBuffer[String]],
                           dealWith : Option[(Int, String) => Unit]
                          ) : Widget[_] = {
    val uuid = UUID.randomUUID().toString



    val enabledField : Var[Boolean] = Var(true)
    def recalcEnability() : Unit = {
      val seq : Seq[String] = containsCheck.map(_.get).getOrElse(Seq.empty)
      val s : String = selectedField.get.getOrElse("N/A")
      enabledField := !seq.contains(s)
    }

    containsCheck.foreach(_.changes.attach(f => recalcEnability()))
    selectedField.attach(f => recalcEnability())

    div(
      Label(labelText).attribute("for", uuid).attribute("class", "col-xs-2 control-label"),
    div(
      div(
        div(
          select().bind(possibleOptions, option, selectedField).attribute("class", "form-control")
        ).attribute("class", "form-group"),
        div(
          Input.Number().attribute("class", "form-control").bind(intFieldStr)
        ).attribute("class", "form-group"),
        div(
          Button(buttonText).onClick(f => {
            dealWith.foreach(func => selectedField.get.foreach(
              selection => Try(Integer.parseInt(intFieldStr.get)).foreach(i => {
                func.apply(i, selection)
              })))
          }).attribute("class", "form-control").enabled(enabledField)
        ).attribute("class", "form-group")
      ).attribute("class", "form-inline")
    ).attribute("class", "col-xs-10").attribute("id", uuid)
    ).attribute("class", "form-group")
  }


  def twoWidgetsAndAButtonInline(labelText : ReadChannel[String], buttonText : ReadChannel[String], buttonAction : () => Unit, buttonEnabled : ReadChannel[Boolean]
                                , widgetA : Widget[_], widgetB : Widget[_]
                                ) : Widget[_] = {
    val uuid = UUID.randomUUID().toString
    div(
      Label(labelText).attribute("for", uuid).attribute("class", "col-xs-2 control-label"),
      div(
        div(
          div(
            widgetA
          ).attribute("class", "form-group"),
          div(
            widgetB
          ).attribute("class", "form-group"),
          div(
            Button(buttonText).onClick(f => buttonAction.apply()).attribute("class", "form-control").enabled(buttonEnabled)
          ).attribute("class", "form-group")
        ).attribute("class", "form-inline")
      ).attribute("class", "col-xs-10").attribute("id", uuid)
    ).attribute("class", "form-group")
  }
}
