


import nephtys.loom.charm.protocol.CharmProtocol.{DescriptionChanged, Event, NameChanged}
import nephtys.loom.charm.protocol.CharmProtocol.Serialization.FailableEventList
import nephtys.loom.charm.protocol.UniquePowerIdentifier
import nephtys.loom.charm.server.Failable
import org.scalatest._
import upickle.default._

/**
  * Created by nephtys on 11/16/16.
  */
class ExampleSpec  extends FunSpec {


  describe("TutorialApp") {
    it("should do one") {
      assert(true)
    }

    it("should do two") {
      assert(true)
    }
    it("should not fail") {
      assert(!false)
    }

    it("should parse a simple event") {
      val json = """[{"success":[],"failure":["Cannot add keyword"]}]"""
      val parsed : Seq[Failable[Event]] = read[Seq[Failable[Event]]](json)
      assert(parsed.size == 1)
      assert(parsed.exists(_.isFailure))
    }


    it("should deseralize a failablelist") {
      val uuid1 : UniquePowerIdentifier = UniquePowerIdentifier("""b6ab76b8-d214-4f35-8fa0-d8d2d1229031""")
      val input : FailableEventList = FailableEventList(List(Left(DescriptionChanged(uuid1, "New description")), Left(NameChanged(uuid1, "new name")), Right("Error")))
      val json : String = """{"results":[[0,{"$type":"nephtys.loom.charm.protocol.CharmProtocol.DescriptionChanged","id":{"id":"b6ab76b8-d214-4f35-8fa0-d8d2d1229031"},"description":"New description"}],[0,{"$type":"nephtys.loom.charm.protocol.CharmProtocol.NameChanged","id":{"id":"b6ab76b8-d214-4f35-8fa0-d8d2d1229031"},"name":"new name"}],[1,"Error"]]}"""
      val serial : String = write[FailableEventList](input)
      println(json)
      println(serial)
      assert(serial.equals(json))
      val deserial : FailableEventList = read[FailableEventList](serial)
      assert(deserial.equals(input))
    }



  }
}