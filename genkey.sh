#!/bin/sh
# This is a comment!
echo "Generating"ch	# This is a comment, too!
openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" -keyout target/cakey.pem -out target/cacert.pem
echo "Done with Generating"
openssl pkcs12 -passout pass: -export -in target/cacert.pem -inkey target/cakey.pem -out target/cert.p12 -name "mykey"
echo "PKCS12 file created in /target/cert.p12"
mkdir jvm
mkdir jvm/target
cp target/cert.p12 jvm/target/cert.p12
echo "also copied to /jvm/target/cert.p12"
