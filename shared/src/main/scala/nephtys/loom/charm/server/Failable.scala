package nephtys.loom.charm.server

import nephtys.loom.charm.protocol.CharmProtocol.Event

import scala.util.Try
import upickle.default._

/**
  * Created by nephtys on 11/24/16.
  */
case class Failable[T](success: Option[T], failure : Option[String]) {
  def isSuccess: Boolean = failure.isEmpty && success.isDefined
  def isFailure: Boolean = !isSuccess
  def asTry : Try[T] = Try(success.getOrElse(throw new Exception(failure.get)))
}

final case class FailableEvent(success : Option[Event], failure : Option[String]) {
  def isSuccess: Boolean = failure.isEmpty && success.isDefined
  def isFailure: Boolean = !isSuccess
  def asTry : Try[Event] = Try(success.getOrElse(throw new Exception(failure.get)))
}
