package nephtys.loom.charm.server.authenticationlayer

import akka.http.scaladsl.server.Route
import nephtys.loom.charm.protocol.Email

import scala.concurrent.Future

/**
  * Created by nephtys on 12/1/16.
  */
class AuthMockModule extends AuthModuleable{
  val myEmail : Email = Email("myemail@site.org")
  val authHeaderValueWithMyEmail : String = ???
  val authHeaderTooOld : String = ???
  val authHeaderInvalid : String = ???
  val authHeaderEmpty : String = ???

  override def loginRoute: Route = ???

  override def redirectRoute: Route = ???

  override def verifyRoute: Route = ???

  override def verify(authHeaderValue: String): Future[Option[Email]] = ???
}
