package nephtys.loom.charm.server.ssl

import java.security.cert.X509Certificate

import akka.http.scaladsl.HttpsConnectionContext
import sun.security.tools.keytool.CertAndKeyGen
import sun.security.x509.X500Name
import java.io.{File, FileInputStream, InputStream}
import java.security.{KeyStore, SecureRandom}
import javax.net.ssl.{KeyManagerFactory, SSLContext, TrustManagerFactory}

import akka.actor.ActorSystem
import akka.http.scaladsl.server.{Directives, Route, RouteResult}
import akka.http.scaladsl.{ConnectionContext, Http, HttpsConnectionContext}
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.typesafe.sslconfig.akka.AkkaSSLConfig

import scala.util.Try

/**
  * Created by nephtys on 11/17/16.
  */
object CertManager {

  private def createKeys(parentdir : String) : String = {
    """openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 """ +
      """-subj "///C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" """ +
    s"""-keyout $parentdir/cakey.pem -out $parentdir/cacert.pem"""
  }
  private def transformToP12(parentdir : String, filepathP12 : String) : String =
    """openssl pkcs12 -passout pass: -export -in """+
      s"""$parentdir/cacert.pem -inkey $parentdir/cakey.pem -out $filepathP12 -name "mykey""""



  //code adapted from http://www.pixelstech.net/article/1406724116-Generate-certificate-in-Java----Self-signed-certificate
  def generateSelfSignedCertificate() : Try[String] = Try{
    //generate parent directory
    val parentdir = new File("target/cert.p12").getParent
    new File(parentdir).mkdirs()
    assert(parentdir.last != '/')

    import sys.process._

    //generate private and public key
    //println("executing: " + createKeys(parentdir))
    //val process1 = Runtime.getRuntime.exec(createKeys(parentdir))
    /*println(Try(Runtime.getRuntime.exec(Array[String](
      "openssl", "req", "-new", "rsa:4096", "-days", "365", "-nodes", "-x509", "-subj",
      """"/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"""", "-keyout" ,"target/cakey.pem",
      "-out", "target/cacert.pem"
    )).waitFor()))*/
    //println(process1.exitValue())
    //println(createKeys(parentdir).!!)
    "./genkey.sh".!!


    //generate pkcs12
    //println("executing: " + transformToP12(parentdir, filepath))
    //val process2 = Runtime.getRuntime.exec(transformToP12(parentdir, filepath))
    //println(transformToP12(parentdir, filepath).!!)
    //process2.wait()

    //filepath
  }


  def buildSSLContextFrom() : HttpsConnectionContext = {
    val keystoreFilepath = ConfigFactory.load().getString("loom.charmserver.certpath")

    val keystorePassword: Array[Char] = ConfigFactory.load().getString("loom.charmserver.certpassword").toCharArray

    val ks: KeyStore = KeyStore.getInstance("PKCS12")
    println("opening stream from File = " + new File(keystoreFilepath).getAbsolutePath)
    val keystore: InputStream = {
      new FileInputStream(keystoreFilepath)
    }

    println("stream opened")
    ks.load(keystore, keystorePassword)

    println("stream loaded")
    val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance("SunX509")
    keyManagerFactory.init(ks, keystorePassword)

    println("key factory initaited")
    val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
    println("trust factory")
    tmf.init(ks)
    println("trust factory initialized")

    //keystore.close()

    val sslContext: SSLContext = SSLContext.getInstance("TLS")
    sslContext.init(keyManagerFactory.getKeyManagers, tmf.getTrustManagers, new SecureRandom())

    println("ssl context initated")
    val https: HttpsConnectionContext = ConnectionContext.https(sslContext)


    println("context created")

      https
  }
}
