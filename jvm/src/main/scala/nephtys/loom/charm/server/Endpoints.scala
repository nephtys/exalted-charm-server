package nephtys.loom.charm.server

import java.util.Date

import akka.http.scaladsl.server._
import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import nephtys.loom.charm.protocol.CharmProtocol.Command
import nephtys.loom.charm.server.authenticationlayer.{AuthModule, AuthModuleable, RouteVerifier}
import nephtys.loom.charm.server.configuration.Configuration
import nephtys.loom.charm.server.datalayer.DataLayer
import upickle.default._

/**
  * Created by nephtys on 11/17/16.
  */
class Endpoints(dataLayer: DataLayer)(implicit authModule : AuthModuleable, configuration : Configuration) {


import RouteVerifier._
  //TODO: charm get
  //TODO: charm post
  val charmRoute : Route = {
    path("charms") {
      get {
        OIDCauthenticated { email => conditional(dataLayer.currentETag){
          println(s"receiving something on /charms-GET by email = $email at date ${new Date()}")
          //respondWithHeader(RawHeader("ETag", dataLayer.currentETag.tag)) {
          complete {
            println(s"sending charms: ${dataLayer.getAllPowers(email)}")
            write(dataLayer.getAllPowers(email))
          }
          //}
        }
        }
      } ~
      post {
        println("receiving something on /charms-POST")
        CSRFCheckForSameOrigin {
          OIDCauthenticated { email =>
            jsonPost[List[Command]] {
              commands => {
                println(s"received commands: $commands")
                onSuccess(dataLayer.doCommands(commands, email)) { s =>
                    complete(write(s))
                }
              }
            }
          }
        }
      }
    }
  }

  //static resources
  val staticRoute : Route = getFromDirectory(configuration.pathToStaticResources)


}
