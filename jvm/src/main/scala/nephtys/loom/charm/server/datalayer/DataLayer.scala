package nephtys.loom.charm.server.datalayer

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.ActorMaterializer
import nephtys.loom.charm.protocol.CharmProtocol.Serialization.FailableEventList
import nephtys.loom.charm.protocol.CharmProtocol.{Command, Event}
import nephtys.loom.charm.protocol.{Email, Power}
import nephtys.loom.charm.server.Failable
import nephtys.loom.charm.server.authenticationlayer.AuthorizedCommand

import scala.collection.immutable.TreeSet
import scala.concurrent.{Future, Promise}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

/**
  * Created by nephtys on 11/17/16.
  */
class DataLayer(implicit val mat : ActorMaterializer, val system : ActorSystem) {

  private val persistentKeeper : ActorRef = system.actorOf(PersistentKeeper.props())

  private val queryModule : QueryModule = new QueryModule()

  def currentETag : akka.http.scaladsl.model.headers.EntityTag = akka.http.scaladsl.model.headers.EntityTag(queryModule.currentETag.toString, weak = false)

  def getAllPowers(player : Email) : TreeSet[Power] = queryModule.aggregateAsSeq.get() //TODO: filtering per user

  protected def enqueueChange( command : AuthorizedCommand, promise : Promise[Event]) : Unit = {
    val message = PersistentKeeper.Request(command, promise)
    persistentKeeper ! message
  }

  protected def enqueueCommands(command : List[Command], authorizer : Email, promise: List[Promise[Event]]) : Unit = {
    assert(command.size == promise.size)
    command.zip(promise).foreach(c => {
      def command = AuthorizedCommand(authorizer, c._1 )
      def promise = c._2
      enqueueChange(command, promise)
    })
  }

  def doCommands(command : List[Command], authorizer : Email) : Future[FailableEventList] = {
    val promises = command.map(s => Promise[Event]())
    val futures : Seq[Future[Try[Event]]] = promises.map(p => {
      val input = p.future
      val output : Promise[Try[Event]] = Promise[Try[Event]]()
        input.onComplete{
          case Success(s) => {
            println(s"Finishing input successfully $s")
            output.trySuccess(Try(s))
          }
          case Failure(e) => {
            println(s"Finishing input failed: $e")
            output.trySuccess(Failure(e))
          }
        }
      println("writing promise")
      output.future
    })
    enqueueCommands(command, authorizer, promises)

    Future.sequence(futures).map(seq => {
      FailableEventList(seq.map {
        case Success(e) => Left(e)
        case Failure(e) => Right(e.getMessage)
      })
    })


  }
}
