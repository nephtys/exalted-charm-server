package nephtys.loom.charm.server.authenticationlayer

import java.util.Date

import akka.http.scaladsl.server.Directives.{get, path, post}
import akka.http.scaladsl.server.Route
import nephtys.loom.charm.protocol.Email
import akka.http.scaladsl.server._
import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.config.ConfigFactory
import nephty.oidc.helper.GoogleAuth
import nephty.oidc.helper.GoogleAuth.RemoteVerifiedIdentityToken
import nephtys.loom.charm.server.configuration.Configuration
import upickle.default._

import scala.concurrent.Future
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by nephtys on 11/17/16.
  */
class AuthModule(implicit val config : Configuration) extends AuthModuleable {

  private val google = new GoogleAuth(GoogleAuth.ClientCredentialFile(
    GoogleAuth.Web(
      ConfigFactory.load().getString("openidconnect.google.client.id"),
      ConfigFactory.load().getString("openidconnect.google.client.project_id"),
      ConfigFactory.load().getString("openidconnect.google.auth_uri"),
      ConfigFactory.load().getString("openidconnect.google.token_uri"),
      ConfigFactory.load().getString("openidconnect.google.auth_provider_x509_cert_url"),
      ConfigFactory.load().getString("openidconnect.google.client.secret")
    )
  ), GoogleAuth.RedirectFile(
    config.redirecturl,
    config.hostname,
    config.port
  ))

  protected object verifyer {

    import scalacache._
    import caffeine._

    protected implicit val scalaCache = ScalaCache(CaffeineCache())

    //TODO: maybe it would be smart to hash incoming headers into smaller cache keys
    def verify(authHeaderValue: String): Future[Option[Email]] = cachingWithTTL(authHeaderValue)(FiniteDuration.apply(1, "minutes")) {
      if (authHeaderValue.startsWith("Bearer ")) {
        val token: String = authHeaderValue.substring("Bearer ".length).trim
        println(s"extracted token before remote verfiy : $token")
        verifyTokenRemote(token).map(verifiedtoken => {
          println("verified token:")
          verifiedtoken.foreach(println)
          verifiedtoken
            .filter(t => t.testIss) //this token comes from google
            .filter(t => t.aud.equals(google.clientCredentials.web.client_id)) //this token is for my application
            .filter(_.notExpired) //this token is not yet expired
            .map(s => Email(s.email))
        })
      } else {
        println("Header does not start with Bearer")
        Future.successful(None)
      }
    }
  }

  def verify(authHeaderValue: String): Future[Option[Email]] = verifyer.verify(authHeaderValue)

  protected def verifyTokenRemote(token: String): Future[Option[RemoteVerifiedIdentityToken]] = {
    Future {
      google.verify(token).toOption
    }
  }

  private val verifyUrl = ConfigFactory.load().getString("loom.charmserver.verify_endpoint")

  def verifyRoute: Route = path(verifyUrl) {
    get {
      RouteVerifier.OIDCauthenticated {
        email => complete(write(email))
      }(this)
    }
  }



  def loginRoute : Route = {
    path(ConfigFactory.load().getString("loom.charmserver.login_endpoint")) {
      get {
        redirect(google.authRequestUrl(), StatusCodes.TemporaryRedirect)
      }
    }
  }
  def redirectRoute : Route = {
    path(ConfigFactory.load().getString("loom.charmserver.redirect_endpoint")) {
      get {
        parameters('state, 'code
          , 'authuser, 'session_state, 'prompt
        ) {(state, code
            , authuser, session_state, prompt
           ) =>
          //TODO: extract state and code query parameters
          //TODO: exchange code for access and ID token via HTTPS POST (with client_id and client_secret)
          println("Received something at redirectedendpoint at " + new Date().toString)
          println(s"code = $code")
          println(s"state = $state")
          val tryaccess = google.postCodeForAccessAndIDToken(code)

          if (tryaccess.isSuccess) {
            val access = tryaccess.get
          println(access)
          onSuccess(verify("Bearer "+access.id_token)) { verified =>
            redirect("/index.html#/login/"+access.id_token, StatusCodes.TemporaryRedirect)
          }
          } else {
            complete {
              "There was an error validating your token, we are sorry about that. Try again"
            }
          }
        }
      }
    }
  }
}
