package nephtys.loom.charm.server.configuration

import com.typesafe.config.ConfigFactory
import nephtys.loom.charm.protocol.Email

/**
  * Created by nephtys on 11/17/16.
  */
class Configuration() {

  def port : Int = ConfigFactory.load().getInt("loom.charmserver.port")
  def pathToStaticResources : String =  ConfigFactory.load().getString("loom.charmserver.staticdirpath")
  def dummyAuthenticate : Option[Email] = Some(Email("dummy.user@dummy.world"))
  def certFileInsteadOfGeneratedWithFilePathAndPassword : Option[(String, String)] = ???

  def isHttps : Boolean = protocol.equals("https")
  def redirect_endpoint : String = ConfigFactory.load().getString("loom.charmserver.redirect_endpoint")
  def hostname : String = ConfigFactory.load().getString("loom.charmserver.raw_host")
  def protocol : String = ConfigFactory.load().getString("loom.charmserver.protocol")
  def completeurl : String = s"""$protocol://$hostname:$port"""
  def redirecturl : String = completeurl + "/"+redirect_endpoint

}
