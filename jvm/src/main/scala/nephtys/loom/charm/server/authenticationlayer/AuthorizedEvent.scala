package nephtys.loom.charm.server.authenticationlayer

import nephtys.loom.charm.protocol.CharmProtocol.Event
import nephtys.loom.charm.protocol.Email

/**
  * Created by nephtys on 11/17/16.
  */
case class AuthorizedEvent(email : Email, event : Event) {

}
