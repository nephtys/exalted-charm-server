package nephtys.loom.charm.server


import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server._
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import com.typesafe.config.ConfigFactory
import nephty.oidc.helper.GoogleAuth
import nephtys.loom.charm.server.authenticationlayer.{AuthModule, AuthModuleable}
import nephtys.loom.charm.server.configuration.Configuration
import nephtys.loom.charm.server.datalayer.DataLayer
import nephtys.loom.charm.server.ssl.CertManager

import scala.concurrent.Future

/**
  * Created by nephtys on 10/3/16.
  */
object Main extends App {

  println("Hello " + ConfigFactory.load().getString("answer.hello"))
  println("Underlying value = " + ConfigFactory.load().getString("answer.underlying"))


  implicit val actorSystem = ActorSystem("system")
  implicit val actorMaterializer = ActorMaterializer()




  //CertManager.generateSelfSignedCertificate()



  implicit val config = new Configuration()
  implicit val authmodule : AuthModuleable = new AuthModule()
  val datalayer = new DataLayer()
  val endpoints = new Endpoints(datalayer)

  println("This ia a console output from the exalted-charm-server, with a modification")


  val route : Route = pathSingleSlash {
      get {
        complete {
          "Hello world from exalted-charm-server"
        }
      }
    } ~ endpoints.charmRoute ~ endpoints.staticRoute ~ authmodule.authRoutes

  import scala.concurrent.ExecutionContext.Implicits.global

  val usesHttps : Boolean = config.isHttps
  if (usesHttps) {
    println("using SSL cert...")
    val https = CertManager.buildSSLContextFrom()
    val x = Http().bindAndHandle(route,config.hostname,config.port, connectionContext = https)
    println("Opened at: " + config.completeurl)
  } else {
    println("binding without SSL...")
    val x = Http().bindAndHandle(route,config.hostname,config.port)
    println("Opened at: " + config.completeurl)
  }

}
