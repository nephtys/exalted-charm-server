package nephtys.loom.charm.server.authenticationlayer

import akka.http.scaladsl.server.Route
import nephtys.loom.charm.protocol.Email
import nephtys.loom.charm.server.configuration.Configuration
import akka.http.scaladsl.Http
import akka.http.scaladsl.server._
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller

import scala.concurrent.Future

/**
  * Created by nephtys on 12/1/16.
  */
trait AuthModuleable {

  //def config : Configuration

  def loginRoute : Route
  def redirectRoute : Route
  def verifyRoute : Route

  def authRoutes : Route = loginRoute ~ redirectRoute ~ verifyRoute

  def verify(authHeaderValue : String) : Future[Option[Email]]
}
