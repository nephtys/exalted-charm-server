package nephtys.loom.charm.server.datalayer

import akka.actor.{ActorRef, Props}
import akka.persistence.{PersistentActor, SnapshotOffer}
import nephtys.loom.charm.protocol.CharmProtocol.Event
import nephtys.loom.charm.protocol.{Power, UniquePowerIdentifier}
import nephtys.loom.charm.server.authenticationlayer.{AuthorizedCommand, AuthorizedEvent}
import nephtys.loom.charm.server.datalayer.PersistentKeeper.{MakeSnapshot, Request}

import scala.concurrent.Promise
import scala.util.{Failure, Success, Try}

/**
  * Created by nephtys on 11/17/16.
  */
//noinspection TypeCheckCanBeMatch
class PersistentKeeper extends PersistentActor{


  private var aggregate : Map[UniquePowerIdentifier, Power] = Map.empty

  override def receiveRecover: Receive = {
    case SnapshotOffer(metadate, snapshot: Map[_, _]) => {
      Try(snapshot.asInstanceOf[Map[UniquePowerIdentifier, Power]]) match {
        case Success(s) => {
          aggregate = s
        }
        case Failure(e) => {
          println(s"Found weird snapshot in LevelDB: $snapshot with error $e")
        }
      }
    }
    case AuthorizedEvent(email, event) => {
      aggregate = event.commit(aggregate)
    }
  }

  override def receiveCommand: Receive = {
    case Request(AuthorizedCommand(email, command), promise) => {
      val t = command.validate(email,aggregate)
      t match {
        case Success(a) => {
          val event : AuthorizedEvent = AuthorizedEvent(email, a)
          persist(event)(ae => aggregate = ae.event.commit(aggregate))
        }
        case _ =>
      }
      val promisedType = promise.complete(t)
    }
    case MakeSnapshot => {
      saveSnapshot(aggregate)
    }
  }

  override def persistenceId: String = PersistentKeeper.persistenceId
}

object PersistentKeeper {
  def props(): Props = Props(new PersistentKeeper())

  val persistenceId: String = "charmpersistenceactor"

  case class Request(authorizedCommand: AuthorizedCommand, promise : Promise[Event])

  case object MakeSnapshot
}