package nephtys.loom.charm.server.datalayer

import java.util.concurrent.atomic.{AtomicLong, AtomicReference}

import akka.NotUsed
import akka.actor.ActorSystem
import akka.persistence.query.{EventEnvelope, PersistenceQuery}
import akka.persistence.query.journal.leveldb.scaladsl.LeveldbReadJournal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import nephtys.loom.charm.protocol.CharmProtocol.Event
import nephtys.loom.charm.protocol.{Email, Power, UniquePowerIdentifier}
import nephtys.loom.charm.server.authenticationlayer.AuthorizedEvent

import scala.collection.immutable.{SortedSet, TreeSet}

/**
  * Created by nephtys on 11/17/16.
  */
class QueryModule(implicit val mat : ActorMaterializer, val system : ActorSystem) {


  def currentETag : Long = lastSequenceNumber.get()

  val aggregate : AtomicReference[Map[UniquePowerIdentifier, Power]] = new AtomicReference[Map[UniquePowerIdentifier,
    Power]](Map.empty)

  implicit val ordering = new Ordering[Power] {
    override def compare(x: Power, y: Power): Int = x.uuid.id.compareTo(y.uuid.id)
  }

  val aggregateAsSeq : AtomicReference[TreeSet[Power]] = new AtomicReference[TreeSet[Power]](TreeSet.empty[Power])

  val emailsCollected : AtomicReference[Set[Email]] = new AtomicReference[Set[Email]](Set.empty)

  val lastSequenceNumber : AtomicLong = new AtomicLong(0)

  private def applyEvent(event : Pack) = {
    println("Applying Event : " + event)
    synchronized {
      aggregate.set(event.authorizedEvent.event.commit(aggregate.get()))
      emailsCollected.set(emailsCollected.get().+(event.authorizedEvent.email))
      lastSequenceNumber.set(event.sequencenumber)
      aggregateAsSeq.set(TreeSet.empty[Power].++(aggregate.get().values))
    }
  }


  private lazy val queries = PersistenceQuery(system).readJournalFor[LeveldbReadJournal](
    LeveldbReadJournal.Identifier)

  private lazy val src: Source[EventEnvelope, NotUsed] =
    queries.eventsByPersistenceId(PersistentKeeper.persistenceId, 0L, Long.MaxValue)

  //TODO: implement snapshot on query side too

  lazy val events: Source[Pack, NotUsed] = src.filter(_.event.isInstanceOf[AuthorizedEvent]).map(toPack)

  events.runForeach(applyEvent)


  def toPack(e : EventEnvelope) : Pack = Pack(e.sequenceNr, e.event.asInstanceOf[AuthorizedEvent])
  case class Pack(sequencenumber : Long, authorizedEvent: AuthorizedEvent)

}
